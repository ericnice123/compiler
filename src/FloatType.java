/**
 * Created by Tsung-Huan on 4/6/15.
 */
class FloatType extends NumericType {
    public FloatType()
    {
        super("float", 4);
    }

    public boolean isFloat()
    {
        return true;
    }

    public boolean isAssignableTo(Type t)
    {
        if(t instanceof FloatType)
            return true;
        else
            return false;
    }

    public boolean isEquivalentTo(Type t) {
        if (t instanceof FloatType) {
            return true;
        } else {
            return false;
        }
    }
}

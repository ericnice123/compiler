/**
 * Created by Dennis on 4/20/2015.
 */
class NullPointerType extends PointerType {
    public NullPointerType()
    {
        super("nullptr", 4);
    }

    public boolean isAssignableTo(Type t)
    {
        if(t instanceof PointerType)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

/**
 * Created by oscarlin on 4/23/15.
 */
public class ThisSTO extends STO {

    public ThisSTO(String id, Type t){
        super(id, t);
    }

    public boolean isThis(){
        return true;
    }
}

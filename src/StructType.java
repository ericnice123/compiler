/**
 * Created by oscarlin on 4/17/15.
 */

import java.util.*;
public class StructType extends Type {
    //Declare all variables and blah here

    private Stack<Scope> st_scope;
    private Scope st_global;
    private String decl_name;
    private List<FuncSTO> decl_func = new ArrayList<FuncSTO>();
    private List<FuncSTO> decl_const = new ArrayList<FuncSTO>();
    private FuncSTO func = null;
    public int offset = 0; //will be use for project 2 and check the size
    private boolean size_known;
    private boolean has_dtor = false;
    private int size = 0;


    public StructType(String id) {
        super("struct", 32);
        decl_name = id;
        st_scope = new Stack<Scope>();
        st_global = null;
    }
    public List<FuncSTO> getFuncList(){
        return decl_func;
    }

    public boolean getHasDtor(){
        return has_dtor;
    }
    public void setDtor(){
        has_dtor=true;
    }
    public void openScope()
    {
        //System.out.println("Struct Open scope");
        Scope scope = new Scope();
        // The first scope created will be the global scope.
        if (st_global == null) {
            st_global = scope;
            st_scope.push(scope);
        }
        else // added code for break and continue
        {
            Scope s = st_scope.peek();
            if (s.getIsLoop()) {
                scope.setIsLoop(true);
            }
            st_scope.push(scope);
        }
    }

    public void closeScope()
    {
        st_scope.pop();
    }

    public void insert(STO sto)
    {
        Scope scope = st_scope.peek();
        if(sto instanceof VarSTO && scope == st_global){

            sto.setOffset(""+offset);
            offset += sto.getType().getSize();
            size += sto.getType().getSize();
            sto.setBase("%g0");
        }
        scope.InsertLocal(sto);
    }

    public STO accessGlobal(String strName)
    {
        return st_global.access(strName);
    }

    public STO access(String strName)
    {
        Stack<Scope> stk = new Stack();
        Scope scope;
        STO stoReturn = null;

        for (Enumeration<Scope> e = st_scope.elements(); e.hasMoreElements();)
        {
            scope = e.nextElement();
            stk.push(scope);
        }

        // fixed, originally there's a bug when the id is not defined, it will still pop the empty stk
        // which caused an error
        while (!stk.empty()) {
            scope = stk.pop();
            if ((stoReturn = scope.access(strName)) != null) {
                return stoReturn;
            }
        }

        return null;
    }

    public STO accessLocal(String strName)
    {
        Scope scope = st_scope.peek();
        return scope.accessLocal(strName);
    }

    public String getName() {
        return decl_name;
    }

    @Override
    public boolean isEquivalentTo(Type t) {
        return t.isStruct() && t.getName().equals(decl_name);
    }

    public boolean isStruct() {
        return true;
    }



    public FuncSTO getFunc(){
        return func;
    }

    public void setFunc(FuncSTO f){

        func = f;
    }

    public int getSize(){
        return size;
    }

    public boolean validFunction(FuncSTO f){
        boolean flag = false;
        int i = 0;
        if(decl_func.size() == 0){
            return false;
        }

        while (!flag && i < decl_func.size()) {
            FuncSTO fd = decl_func.get(i);
            if (fd.getName().equals(f.getName())) {
                if(fd.getParamList()!= null && f.getParamList() != null) {
                    if (fd.getParamList().size() == f.getParamList().size()) {
                        if (fd.getParamList().size() != 0) {
                            boolean overload = true;
                            for (int j = 0; j < fd.getParamList().size(); j++) {
                                Type fdtype = fd.getParamList().get(j).getType();
                                Type ftype = f.getParamList().get(j).getType();
                                if(fd.getParamList().get(j).isRef()){
                                    if(!(f.getParamList().get(j).isModLValue())){
                                        if(!(ftype instanceof ArrayType || ftype instanceof StructType)) {
                                            flag = false;
                                            return flag;
                                        }
                                    }
                                }
                                if (!(ftype.isEquivalentTo(fdtype))) {
                                    overload = false;
                                }
                            }
                            if(overload){
                                flag = true;
                                break;
                            }
                        } else {
                            flag = true;
                            break;
                        }
                    }
                }
                else if(fd.getParamList()==null && f.getParamList() == null ||
                              fd.getParamList().size() == 0 && f.getParamList() == null){
                    flag = true;
                    break;
                }

            }
            i++;
        }
        return flag;
    }

    public boolean validInstantiation(FuncSTO f){
        boolean flag = false;
        int i = 0;
        if(decl_const.size() == 0){
            return false;
        }

        while (!flag && i < decl_const.size()) {
            FuncSTO fd = decl_const.get(i);
            if (fd.getName().equals(f.getName())) {
                if(fd.getParamList()!= null && f.getParamList() != null) {
                    if (fd.getParamList().size() == f.getParamList().size()) {
                        if (fd.getParamList().size() != 0) {
                            boolean overload = true;
                            for (int j = 0; j < fd.getParamList().size(); j++) {
                                Type fdtype = fd.getParamList().get(j).getType();
                                Type ftype = f.getParamList().get(j).getType();
                                if (!(ftype.isEquivalentTo(fdtype))) {
                                    overload = false;
                                }
                            }
                            if(overload){
                                flag = true;
                                break;
                            }
                        } else {
                            flag = true;
                            break;
                        }
                    }
                }
                else if(fd.getParamList()==null && f.getParamList() == null){
                    flag = true;
                    break;
                }
            }
            i++;
        }
        return flag;
    }

    public boolean validConstructor(FuncSTO f){
        if(!(f.getType().isStruct())){
            return false;
        }
        if(decl_const.size() == 0){
            decl_const.add(f);
            return true;
        }
        if(!validInstantiation(f)){
            decl_const.add(f);
            return true;
        }
        return false;

    }

    public boolean illegalFunc(FuncSTO f){
        boolean flag = false;
        int i = 0;
        boolean isOverload = true;
        FuncSTO fdt = null;
        if(decl_func.size() == 0){
            decl_func.add(f);
            return false;
        }
        while (!flag && i < decl_func.size()) {
            FuncSTO fd = decl_func.get(i);
            if (fd.getName().equals(f.getName())) {
                if(fd.getParamList()!= null && f.getParamList() != null) {
                    if (fd.getParamList().size() == f.getParamList().size()) {
                        if (fd.getParamList().size() != 0) {
                            for (int j = 0; j < fd.getParamList().size(); j++) {
                                Type fdtype = fd.getParamList().get(j).getType();
                                Type ftype = f.getParamList().get(j).getType();
                                if (fdtype.isEquivalentTo(ftype)) {
                                    flag = true;
                                }
                            }
                        } else {
                            flag = true;
                        }
                    }
                }
                else if(fd.getParamList()==null && f.getParamList() == null){
                    flag = true;
                }

                //overload ok
                if (flag) {
                    isOverload = false;
                }
                fdt = fd;
            }
            i++;
        }

        if(!flag){
            if(fdt != null && isOverload){
                fdt.setOverload(true);
                f.setOverload(true);
            }
            decl_func.add(f);
        }
        return flag;

    }

    public boolean sizeIsKnown(){
        return size_known;
    }

    public void Finish(){
        size_known = true;
    }

    public List<FuncSTO> getDecl_const(){
        return decl_const;
    }

    public void addConstructor(FuncSTO f){
        decl_const.add(f);
    }

    public void setScopeIsLoop(boolean b)
    {
        Scope s = st_scope.peek();
        s.setIsLoop(b);
    }

}

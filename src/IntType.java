/**
 * Created by Tsung-Huan on 4/6/15.
 */
class IntType extends NumericType {
    public IntType() {
        super("int", 4);
    }

    public boolean isInt() {
        return true;
    }

    public boolean isAssignableTo(Type t) {
        if (t instanceof IntType || t instanceof FloatType)
            return true;
        else
            return false;
    }

    public boolean isEquivalentTo(Type t) {
        if (t instanceof IntType) {
            return true;
        } else {
            return false;
        }
    }
}

//---------------------------------------------------------------------
// CSE 131 Reduced-C Compiler Project
// Copyright (C) 2008-2015 Garo Bournoutian and Rick Ord
// University of California, San Diego
//---------------------------------------------------------------------

import java_cup.runtime.*;
import sun.reflect.annotation.ExceptionProxy;

import java.text.Format;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import java.util.List;
import java.util.*;

class MyParser extends parser
{
	private Lexer m_lexer;
	private ErrorPrinter m_errors;
	private boolean m_debugMode;
	private boolean m_declared = false;
	private int m_nNumErrors;
	private String m_strLastLexeme;
	private boolean m_bSyntaxError = true;
	private int m_nSavedLineNum;
	private List<FuncSTO> declaredFunctions = new ArrayList<FuncSTO>();
	private StructType struct_type;

	private SymbolTable m_symtab;
	public static AssemblyCodeGenerator m_acg;

	public static int offset;
	public static int param_counter;
	public static int cmp_counter;
	public static int else_counter;
	public static int localCounter = 0;
	public static int shortCircuitCounter;
	public static int whileCounter;
	public static Stack<Integer> elseCounterStack = new Stack<Integer>();
	public static Stack<Integer> whileCounterStack = new Stack<>();
	public Stack<String> global_static_VarStack = new Stack<>();
	private String methodName;
	private String returnType;
	private String currentParamTypes = "";
	private String currentGlobalVar;
	public static boolean varIsStatic = false;
	private boolean hasInit;


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public MyParser(Lexer lexer, ErrorPrinter errors, boolean debugMode)
	{
		m_lexer = lexer;
		m_symtab = new SymbolTable();
		m_errors = errors;
		m_debugMode = debugMode;
		m_nNumErrors = 0;
		m_acg = new AssemblyCodeGenerator("rc.s"); // might need to change it
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean Ok()
	{
		return m_nNumErrors == 0;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Symbol scan()
	{
		Token t = m_lexer.GetToken();
		//	We'll save the last token read for error messages.
		//	Sometimes, the token is lost reading for the next
		//	token which can be null.
		m_strLastLexeme = t.GetLexeme();

		switch (t.GetCode())
		{
			case sym.T_ID:
			case sym.T_ID_U:
			case sym.T_STR_LITERAL:
			case sym.T_FLOAT_LITERAL:
			case sym.T_INT_LITERAL:
				return new Symbol(t.GetCode(), t.GetLexeme());
			default:
				return new Symbol(t.GetCode());
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void syntax_error(Symbol s)
	{
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void report_fatal_error(Symbol s)
	{
		m_nNumErrors++;
		if (m_bSyntaxError)
		{
			m_nNumErrors++;

			//	It is possible that the error was detected
			//	at the end of a line - in which case, s will
			//	be null.  Instead, we saved the last token
			//	read in to give a more meaningful error 
			//	message.
			m_errors.print(Formatter.toString(ErrorMsg.syntax_error, m_strLastLexeme));
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void unrecovered_syntax_error(Symbol s)
	{
		report_fatal_error(s);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void DisableSyntaxError()
	{
		m_bSyntaxError = false;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void EnableSyntaxError()
	{
		m_bSyntaxError = true;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String GetFile()
	{
		return m_lexer.getEPFilename();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public int GetLineNum()
	{
		return m_lexer.getLineNumber();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void SaveLineNum()
	{
		m_nSavedLineNum = m_lexer.getLineNumber();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public int GetSavedLineNum()
	{
		return m_nSavedLineNum;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoProgramStart()
	{
		// Opens the global scope.
		m_symtab.openScope();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoProgramEnd() {
		m_acg.dispose();
		m_symtab.closeScope();
	}

	void DoStructInstantiation(String id, Type t, Vector<STO> array, Object ctorParam, boolean isStatic){
		if (m_symtab.accessLocal(id) != null) {
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
			return;
		}
		Vector<STO> pp = (Vector<STO>) ctorParam;
		Vector<STO> ctorP = new Vector<STO>();
		if(pp == null || pp.size() == 0){
			ctorP = new Vector<STO>();
		}
		else {
			for (int i = 0; i < pp.size(); i++){
				ctorP.add(pp.get(i));
			}
		}
		StructType st = (StructType) t;
		FuncSTO tmp = new FuncSTO(st.getName(), st, m_symtab.getLevel()+1);
		tmp.setParam(ctorP);
		if(!isStatic || !m_symtab.isGlobal()) {
			//m_acg.writeComment("Store Parameter");
		}
		m_acg.writeAssembly("\n");

		List<FuncSTO> constructList = st.getDecl_const();
		if(st.validInstantiation(tmp)) {
			DoVarDecl(id, st, array, null);
		}
		else{
			if(constructList.size() == 1) {
				int size = 0;
				int cpsize = 0;
				if(constructList.get(0).getParamList() != null){
					size = constructList.get(0).getParamList().size();
				}
				if(ctorP != null){
					cpsize = ctorP.size();
				}
				if(size == cpsize){
					DoVarDecl(id, st, array, null);
					return;
				}

				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error5n_Call, cpsize, size));
				return;
			}
			else{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error9_Illegal, st.getName())); //Followed reference compiler
				return;
			}
		}
		if(isStatic){
			boolean init = false;
			if(m_symtab.getFunc() != null){
				id = this.methodName + "." + id;
				init = true;
			}
			m_acg.writeStaticStructParam(id, st, ctorP, array, false, init);
			global_static_VarStack.push(id);
		}
		else if(m_symtab.isGlobal()){
			m_acg.writeStaticStructParam(id, st, ctorP, array, true, false);
			global_static_VarStack.push(id);
		}
		else if(array != null){
			m_acg.writeStructParam(id, st, ctorP, array, "" + offset, "%fp", false);
		}
		else {
			m_acg.writeStructParam(id, st, ctorP,array, ""+offset, "%fp", false);
		}
	}


	STO DoParameterDecl(String id, Type t, Vector<STO> index, boolean ref){
		if(struct_type!=null){
			if(struct_type.accessLocal(id) != null){
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
				return new ErrorSTO(id);
			}
		}
		else if (m_symtab.accessLocal(id) != null)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
			return new ErrorSTO(id);
		}

		// checking if it is array
		boolean isArray = false;
		if(index != null) isArray = true;

		// first pass, checking if it has error
		boolean noError = true;
		if(isArray)
		{
			for(int i = 0; i < index.size(); i++)
			{
				if(index.get(i) instanceof ErrorSTO)
				{
					noError = false;
				}
			}
		}

		// second pass, checking if they are all IntType and is ConstSTO
		boolean allIndexAreFine = true;
		if(noError && isArray)
		{
			for(int i = 0; i < index.size(); i++)
			{
				STO currentIndex = index.get(i);
				if(!(currentIndex.getType() instanceof IntType))
				{
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error10i_Array, currentIndex.getType().getName()));
					return new ErrorSTO(id);
				}
				else if(!(currentIndex instanceof ConstSTO))
				{
					m_nNumErrors++;
					m_errors.print(ErrorMsg.error10c_Array);
					return new ErrorSTO(id);
				}
				else // when index is ConstSTO with IntType
				{
					if(!(((ConstSTO) currentIndex).getIntValue() > 0)) {
						m_nNumErrors++;
						m_errors.print(Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO) currentIndex).getIntValue()));
						return new ErrorSTO(id);
					}
				}
			}
		}

		// last pass, create array VarSTO
		if(allIndexAreFine && noError && isArray)
		{
			Vector<ArrayType> temp = new Vector<>();
			for(int i = 0; i < index.size(); i++)
			{
				ArrayType at = new ArrayType(4 * ((ConstSTO)index.get(i)).getIntValue());
				at.setDimension(((ConstSTO)index.get(i)).getIntValue());
				temp.add(at);
			}
			// setting type name ex. int[2][2]
			for(int i = 0; i < temp.size(); i++)
			{
				ArrayType cur = temp.get(i);
				String typeName = t.getName()+"["+cur.getDimension()+"]";
				for(int j = (i+1); j < temp.size(); j++)
				{
					ArrayType rest = temp.get(j);
					typeName = typeName+"["+rest.getDimension()+"]";
				}
				cur.setName(typeName);
			}
			for(int i = 0; i < temp.size(); i++)
			{
				if(i != temp.size() -1)
				{
					ArrayType at = temp.get(i);
					at.setElementType(temp.get(i+1));
				}
				else
				{
					ArrayType at = temp.get(i);
					at.setElementType(t);
				}
			}
			Type head = temp.get(0);
			VarSTO sto = new VarSTO(id, head);
			sto.setRef(ref);
			sto.setBase("%fp");
			int tmp;
			if(struct_type!=null){
				tmp = (param_counter+1)*4 + 68;
			}
			else{
				tmp = param_counter*4 + 68;
			}
			sto.setOffset("" + tmp);
			param_counter++;
			return sto;
		}
		else {
			VarSTO sto = new VarSTO(id, t);
			sto.setRef(ref);
			sto.setBase("%fp");
			int tmp;
			if(struct_type!=null){
				tmp = (param_counter+1)*4 + 68;
			}
			else{
				tmp = param_counter*4 + 68;
			}
			sto.setOffset("" + tmp);
			param_counter++;
			return sto;
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	boolean DoVarDecl(String id, Type t, Vector<STO> index, STO opInit)
	{
		if(struct_type != null){
			if(struct_type.accessLocal(id)!=null){
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error13a_Struct, id));
				return false;
			}
		}
		else if (m_symtab.accessLocal(id) != null)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
			return false;
		}

		// checking if it is array
		boolean isArray = false;
		if(index != null) isArray = true;

		// checking assignment
		if(opInit != null)
		{
			if(opInit instanceof ErrorSTO) return false;
			if(isArray)
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error8_Assign, opInit.getType().getName(), t.getName()));
				return false;
			}
			else if(!(opInit.getType().isAssignableTo(t)))
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error8_Assign, opInit.getType().getName(), t.getName()));
				return false;
			}
		}

		// first pass, checking if it has error
		boolean noError = true;
		if(isArray)
		{
			for(int i = 0; i < index.size(); i++)
			{
				if(index.get(i) instanceof ErrorSTO)
				{
					noError = false;
				}
			}
		}

		// second pass, checking if they are all IntType and is ConstSTO
		boolean allIndexAreFine = true;
		if(noError && isArray)
		{
			for(int i = 0; i < index.size(); i++)
			{
				STO currentIndex = index.get(i);
				if(!(currentIndex.getType() instanceof IntType))
				{
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error10i_Array, currentIndex.getType().getName()));
					//allIndexAreFine = false;
					return false;
					//break;
				}
				else if(!(currentIndex instanceof ConstSTO))
				{
					m_nNumErrors++;
					m_errors.print(ErrorMsg.error10c_Array);
					//allIndexAreFine = false;
					return false;
					//break;
				}
				else // when index is ConstSTO with IntType
				{
					if(!(((ConstSTO) currentIndex).getIntValue() > 0)) {
						m_nNumErrors++;
						m_errors.print(Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO) currentIndex).getIntValue()));
						//allIndexAreFine = false;
						return false;
						//break;
					}
				}
			}
		}

		// last pass, create array VarSTO
		if(allIndexAreFine && noError && isArray)
		{
			Vector<ArrayType> temp = new Vector<>();
			for(int i = 0; i < index.size(); i++)
			{
				ArrayType at = new ArrayType(((ConstSTO)index.get(i)).getIntValue());
				at.setDimension(((ConstSTO)index.get(i)).getIntValue());
				temp.add(at);
			}
			// setting type name ex. int[2][2]
			for(int i = 0; i < temp.size(); i++)
			{
				ArrayType cur = temp.get(i);
				String typeName = t.getName()+"["+cur.getDimension()+"]";

				// setting up name and size here
				int tempSize = t.getSize();
				for(int k = i; k < temp.size(); k++)
				{
					ArrayType temp2 = temp.get(i);
					tempSize *= temp2.getSize();
				}
				cur.setSize(tempSize);

				for(int j = (i+1); j < temp.size(); j++)
				{
					ArrayType rest = temp.get(j);
					typeName = typeName+"["+rest.getDimension()+"]";
				}
				cur.setName(typeName);
			}
			for(int i = 0; i < temp.size(); i++)
			{
				if(i != temp.size() -1)
				{
					ArrayType at = temp.get(i);
					at.setElementType(temp.get(i+1));
				}
				else
				{
					ArrayType at = temp.get(i);
					at.setElementType(t);
				}
			}
			Type head = temp.get(0);
			VarSTO sto = new VarSTO(id, head);
			if(!m_symtab.isGlobal())
			{
				if(varIsStatic) // internal Static Array
				{
					sto.setOffset(methodName+"."+id);
					sto.setBase("%g0");
				}
				else
				{
					this.offset -= head.getSize();
					sto.setOffset("" + this.offset);
					sto.setBase("%fp");
				}
			}
			else
			{
				sto.setOffset(id);
				sto.setBase("%g0");
			}
			if(struct_type==null) {
				m_symtab.insert(sto);
				return true;
			}
			else{
				sto.strucOffset = struct_type.offset;
				struct_type.insert(sto);  //if it's in struct, added to struct
				return true;
			}
		}
		else {
			VarSTO sto = new VarSTO(id, t);
			if(!m_symtab.isGlobal()) {
				if(varIsStatic) // internal static variables
				{
					sto.setOffset(methodName+"."+id);
					sto.setBase("%g0");
				}
				else
				{
					this.offset -= t.getSize();

					sto.setOffset("" + this.offset);
					sto.setBase("%fp");
				}
			}
			else
			{
				sto.setOffset(id);
				sto.setBase("%g0");
			}
			if(struct_type==null) {
				m_symtab.insert(sto);
				return true;
			}
			else{
				// print global Assembly
				sto.strucOffset = struct_type.offset;
				struct_type.insert(sto);
				return true;
			}
		}
	}

	// GlobalDefine
	void DoGlobalDefine(boolean isStatic, String id, Type t, Vector<STO> index)
	{
		if(m_symtab.isGlobal()) // or internal static
		{
			if(index != null) {
				STO result = getArraySize(index);
				int sizeOfArray = ((ConstSTO)result).getIntValue();
				global_static_VarStack.push(id);
				m_acg.writeGlobalDefine(isStatic, id, t, true, ""+sizeOfArray);
			}
			else
			{
				global_static_VarStack.push(id);
				m_acg.writeGlobalDefine(isStatic, id, t, false, "0");
			}
		}
		else
		{
			if(isStatic) // internal static header
			{
				varIsStatic = isStatic;
				if(index != null) {
					STO result = getArraySize(index);
					int sizeOfArray = ((ConstSTO)result).getIntValue();
					global_static_VarStack.push(methodName+"."+id);
					m_acg.writeGlobalDefine(isStatic, methodName+"."+id, t, true, "" + sizeOfArray);
				}
				else
				{
					global_static_VarStack.push(methodName+"."+id);
					m_acg.writeGlobalDefine(isStatic, methodName+"."+id, t, false, "0");
				}
			}
		}
	}

	void DoWriteGlobalInit()
	{
		if(m_symtab.isGlobal()) {
			currentGlobalVar = global_static_VarStack.peek();
			m_acg.writeGlobalInit(currentGlobalVar);
		}
	}

	void DoWriteInternalInit(boolean isInit)
	{
		hasInit = isInit;
		if(isInit) {
			if (!m_symtab.isGlobal()) {
				if (varIsStatic) {
					currentGlobalVar = global_static_VarStack.peek();
					m_acg.writeInternalInit(currentGlobalVar);
				}
			}
		}
	}

	void DoWriteInternalInitEnd()
	{
		if(hasInit) {
			if (!m_symtab.isGlobal()) {
				if (varIsStatic) {
					currentGlobalVar = global_static_VarStack.pop();
					m_acg.writeInternalInitEnd(currentGlobalVar);
				}
			}
		}
		varIsStatic = false;
		hasInit = false;
	}

	void DoWriteGlobalInitEnd()
	{
		if(m_symtab.isGlobal())
		{
			currentGlobalVar = global_static_VarStack.pop();
			m_acg.writeGlobalInitEnd(currentGlobalVar);
		}
	}

	// method that write global var decl into assembly file
	void DoAsVarDecl(boolean isStatic, String id, Type t, Vector<STO> index, STO opInit) {
		varIsStatic = isStatic;
		boolean noError = DoVarDecl(id, t, index, opInit);
		if(noError) // if no error, successfully insert into symboltable
		{
			if(opInit != null) // if has init value
			{
				if(!(t instanceof StructType))
				{
					if(!(t instanceof FloatType))
					{
						// case for unknown var int or bool
						if(!(opInit instanceof ConstSTO))
						{
							initUnknownVar(isStatic, id, t, opInit);
						}
						else // case for ConstSTO, has value
						{
							initConstValue(isStatic, id, opInit);
						}
					}
					else // t is float type
					{
						if(!(opInit instanceof ConstSTO))
						{
							Type opinitType = opInit.getType();
							if(opinitType instanceof FloatType)
							{
								initUnknownFloat(isStatic, id, t, opInit);
							}
							else // if int type (int 2 float)
							{
								initUnknownInt2Float(isStatic, id, t, opInit);
							}
						}
						else // init value is constSTO
						{
							initFloatConst(isStatic, id, opInit);
						}
					}
				}
			}
		}
	}


	void DoForeachDecl(Type t, boolean ref, String id, STO expr)
	{
		if(!(expr.getType() instanceof ArrayType))
		{
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error12a_Foreach);
		}
		else if(!ref) // default
		{
			ArrayType exprtype = (ArrayType)expr.getType();
			// if expr element type is not assignable to iterationVar
			if(!(exprtype.getElementType().isAssignableTo(t))) {
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error12v_Foreach, exprtype.getElementType().getName(), id, t.getName()));
			}
			else
			{
				VarSTO var = new VarSTO(id, t);
				MyParser.offset -= t.getSize();
				var.setOffset("" + MyParser.offset);
				var.setBase("%fp");
				m_symtab.insert(var);
				MyParser.offset -= t.getSize();
				String lastOffset = ""+MyParser.offset;
				MyParser.m_acg.writeForeach(var, expr, lastOffset);
			}
		}
		else if(ref) // passed by reference
		{
			ArrayType exprtype = (ArrayType)expr.getType();
			if(!(exprtype.getElementType().isEquivalentTo(t))) {
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error12r_Foreach, exprtype.getElementType().getName(), id, t.getName()));
			}
			else
			{
				VarSTO var = new VarSTO(id, t);
				MyParser.offset -= t.getSize();
				var.setOffset(""+MyParser.offset);
				var.setBase("%fp");
				var.setAccessFromArray(true);
				var.setRef(ref);
				m_symtab.insert(var);
				MyParser.offset -= t.getSize();
				String lastOffset = ""+MyParser.offset;
				MyParser.m_acg.writeForeach(var, expr, lastOffset);
			}
		}
		//VarSTO var = new VarSTO(id, t);
		//m_symtab.insert(var);
	}


	// Added method for pointer decl
	Type DoPointerDecl(Type t, Vector<Type> pointers) {
		if (pointers == null) return t;

		for (int i = 0; i < pointers.size(); i++)
		{
			Type type;
			if(i == 0)
			{
				type = pointers.get(i);
				((PointerType)type).setElementType(t);
				((PointerType)type).setName(t.getName() + "*");
			}
			else
			{
				type = pointers.get(i);
				((PointerType)type).setElementType(pointers.get(i-1));
				((PointerType)type).setName(pointers.get(i - 1).getName() + "*");
			}
		}
		return pointers.get(pointers.size() - 1);
	}

	// Added method for * dereference
	STO DoStarDeref(STO des, Operator op)
	{
		if(des instanceof ErrorSTO) return des;

		STO result = op.checkOperands(des);

		if(result instanceof ErrorSTO)
		{
			m_nNumErrors++;
			if(result.getName().equals("nullptr"))
			{
				m_errors.print(ErrorMsg.error15_Nullptr);
			}
			else if(result.getName().equals("notPointerType"))
			{
				m_errors.print(Formatter.toString(ErrorMsg.error15_Receiver, des.getType().getName()));
			}
		}

		return result;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoExternDecl(String id, Type t, Vector<STO> index)
	{
		if (m_symtab.accessLocal(id) != null)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
		}

		DoVarDecl(id, t, index, null);
		//VarSTO sto = new VarSTO(id);
		//m_symtab.insert(sto);
	}

	void DoExternFunction(String id, Type t,  Vector<STO> param){
		FuncSTO f = new FuncSTO(id, t, 0);
		f.setParam(param);
		f.isExtern = true;
		m_symtab.insert(f);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoConstDecl(boolean isStatic, String id, Type t, STO result)
	{
		if (m_symtab.accessLocal(id) != null)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
		}

		ConstSTO sto;

		if(result instanceof ConstSTO) {

			Type resultType = result.getType();

			if(resultType.isAssignableTo(t)) {
				if(t instanceof IntType) {
					sto = new ConstSTO(id, t, ((ConstSTO) result).getIntValue());
					sto.setIsAddressable(true);
					if(!m_symtab.isGlobal()) {
						this.offset -= t.getSize();
						sto.setOffset("" + this.offset);
						sto.setBase("%fp");
					}
					else
					{
						sto.setOffset(id);
						sto.setBase("%g0");
					}

					m_symtab.insert(sto);

					if (m_symtab.isGlobal()) {
						m_acg.initGlobalBasic(id, "" + ((ConstSTO) result).getIntValue(), false, isStatic);
					}
					else
					{
						STO localVar = m_symtab.access(id);
						String initValue = ""+((ConstSTO) result).getIntValue();
						m_acg.writeComment(id+" = "+initValue);
						m_acg.initConst(localVar, initValue);
					}
				}
				else if(t instanceof FloatType) {
					sto = new ConstSTO(id, t, ((ConstSTO) result).getFloatValue());
					sto.setIsAddressable(true);
					if(!m_symtab.isGlobal()) {
						this.offset -= t.getSize();
						sto.setOffset("" + this.offset);
						sto.setBase("%fp");
					}
					else
					{
						sto.setOffset(id);
						sto.setBase("%g0");
					}

					m_symtab.insert(sto);
					if(m_symtab.isGlobal()) {
						m_acg.initGlobalBasic(id, "" + ((ConstSTO) result).getFloatValue(), true, isStatic);
					}
					else
					{
						STO localVar = m_symtab.access(id);
						String initValue = ""+((ConstSTO) result).getFloatValue();
						m_acg.writeComment(id+" = " + initValue);
						m_acg.initFloat(localVar, initValue);
					}
				}
				else if(t instanceof BoolType)
				{
					if(((ConstSTO) result).getBoolValue()) {
						sto = new ConstSTO(id, t, 1);
						sto.setIsAddressable(true);
						m_symtab.insert(sto);

						if(!m_symtab.isGlobal()) {
							this.offset -= t.getSize();
							sto.setOffset("" + this.offset);
							sto.setBase("%fp");
						}
						else
						{
							sto.setOffset(id);
							sto.setBase("%g0");
						}

						if(m_symtab.isGlobal()) {
							m_acg.initGlobalBasic(id, "1", false, isStatic);
						}
						else
						{
							STO localVar = m_symtab.access(id);
							String localOffset = localVar.getOffset();
							m_acg.writeComment(id+" = "+"true");
							m_acg.initConst(localVar, "1");
						}
					}
					else {
						sto = new ConstSTO(id, t, 0);
						sto.setIsAddressable(true);
						m_symtab.insert(sto);

						if(!m_symtab.isGlobal()) {
							this.offset -= t.getSize();
							sto.setOffset("" + this.offset);
							sto.setBase("%fp");
						}
						else
						{
							sto.setOffset(id);
							sto.setBase("%g0");
						}

						if(m_symtab.isGlobal()) {
							m_acg.initGlobalBasic(id, "0", false, isStatic);
						}
						else
						{
							STO localVar = m_symtab.access(id);
							m_acg.writeComment(id+" = "+"false");
							m_acg.initConst(localVar, "0");
						}
					}
				}
			}
			else // not assignable
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error8_Assign, resultType.getName(), t.getName()));
			}
		}
		else // if it is ExprSTO
		{
			if(!(result instanceof ErrorSTO)) {
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error8_CompileTime, id));
			}
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoStructdefDecl(String id)
	{
		if (m_symtab.accessLocal(id) != null)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
		}

		StructType type = new StructType(id);
		struct_type = type;
		struct_type.openScope();

	}

	void DoFinishStructdef(String id){
		struct_type.Finish();
		StructdefSTO sto = new StructdefSTO(id, struct_type);
		struct_type = null;
		m_symtab.insert(sto);
	}

	//----------------------------------------------------------------
	//Modify by Oscar 4/13
	//----------------------------------------------------------------
	void DoFuncDecl_1(String id, Type typ, boolean optref)
	{
		if(struct_type != null && struct_type.access(id)!= null &&
				!(struct_type.access(id).isFunc())){
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error13a_Struct, id));
			struct_type.openScope();
			return;
		}
		else if (m_symtab.accessLocal(id) != null && !(m_symtab.accessLocal(id).isFunc()))
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
			return;
		}

		FuncSTO sto = new FuncSTO(id, typ, m_symtab.getLevel()+1);
		if(m_symtab.accessGlobal(id) != null){
			m_declared = true;
		}
		sto.setRef(optref);
		if(struct_type == null) {
			m_symtab.insert(sto);
			m_symtab.openScope();
			m_symtab.setFunc(sto);
			param_counter=0;
			//DoParameters(params); //To add the parameters to the scope of the function

		}
		else {
			sto.setStruct_member(true);
			struct_type.insert(sto);
			struct_type.openScope();
			struct_type.setFunc(sto);
			param_counter=0;
			//DoParameters(params);
		}
	}

	void DoParameters(Vector<STO> p){
		if(p == null) p = new Vector<STO>();

		for (int i = 0; i < p.size(); i++) {
			currentParamTypes += "." + p.get(i).getType().getName();
		}


		if(struct_type == null) {
			if(p.size()==0){
				return;
			}
			//m_acg.writeComment("Store Param");
			for (int i = 0; i < p.size(); i++) {
				STO sto = p.get(i);
				String param = "%i" + i;
				int offset = 68 + i*4;
				if (m_symtab.accessLocal(sto.getName()) != null) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, sto.getName()));
					m_symtab.setFunc(null);
					return;
				}
				else{
					m_symtab.insert(sto);
					if(sto.getType() instanceof FloatType && !sto.isRef()){
						param = "%f" + i;
					}
					m_acg.writeParam(param, offset);
				}
			}
			m_acg.writeAssembly("\n");
			if(m_symtab.getFunc()!= null) {
				m_symtab.getFunc().setParam(p);
			}
		}
		else{
			//m_acg.writeComment("Store Param");
			m_acg.writeParam("%i0", 68);
			if(p == null){
				return;
			}
			for(int i = 0; i < p.size(); i++){
				STO sto = p.get(i);
				int t = i+1;
				String param = "%i" + t;
				int offset = 68 + t*4;
				if(struct_type.accessLocal(sto.getName())!= null){
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, sto.getName()));
					return;
				}
				else{
					struct_type.insert(sto);
					if(sto.getType() instanceof FloatType){
						param = "%f" + t;
					}
					m_acg.writeParam(param, offset);
				}
			}
			m_acg.writeAssembly("\n");
			if(struct_type.getFunc()!=null) {
				struct_type.getFunc().setParam(p);
			}
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoFuncDecl_2()
	{
		if(struct_type==null) {
			FuncSTO func = m_symtab.getFunc();
			if(func == null){
				return;
			}

			if(!(func.getReturnType().isVoid()) &&
					!(func.hasReturn())){
				if(!(func.getType().isStruct()) && !(func.hasReturn())) {
					m_nNumErrors++;
					m_errors.print(ErrorMsg.error6c_Return_missing);
				}
			}

			boolean flag = false;
			int i = 0;
			boolean isOverload = true;
			FuncSTO fdt = null;
			//Not struct func check
			while (!flag && i < declaredFunctions.size()) {
				FuncSTO fd = declaredFunctions.get(i);

				if (fd.getName().equals(func.getName())) {
					if (fd.getParamList().size() == func.getParamList().size()) {
						if(fd.getParamList().size() != 0) {
							for (int j = 0; j < fd.getParamList().size(); j++) {
								Type fdtype = fd.getParamList().get(j).getType();
								Type ftype = func.getParamList().get(j).getType();
								if (fdtype.isEquivalentTo(ftype)) {
									flag = true;
								}
							}
						}
						else{
							flag = true;
						}
					}
					if (flag) {
						isOverload = false;
					}
					fdt = fd;
				}
				i++;
			}
			if(!flag) {
				if(fdt!=null && isOverload) {
					fdt.setOverload(true);
					func.setOverload(true);
				}
				func.setAssemblyName(this.methodName);
				declaredFunctions.add(func);
			}
			else{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error9_Decl, func.getName()));
			}

			m_acg.writeEndFunction(this.methodName, String.valueOf(-offset));
			if(m_symtab.getFunc().getName().equals("main")){
				m_acg.writeGSDtor();
			}
			m_symtab.setFunc(null);

		}

		//Struct func check
		else {
			FuncSTO func = struct_type.getFunc();
			if (func == null) {
				return;
			}
			boolean flag = false;
			if (func.getType().isStruct()) {
				flag = !(struct_type.validConstructor(func));
			} else {
				flag = struct_type.illegalFunc(func);
			}
			if (flag) {
				m_nNumErrors++;
				if (func.getType().isStruct()) {
					m_errors.print(Formatter.toString(ErrorMsg.error9_Decl, func.getName()));
				} else {
					m_errors.print(Formatter.toString(ErrorMsg.error9_Decl, func.getName()));
				}
				return;
			}
			struct_type.setFunc(null);
			m_acg.writeEndFunction(this.methodName, String.valueOf(-offset)); //fix later
		}

	}

	void DoConstructorDef(String id){
		if(!(struct_type.getName().equals(id))){
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error13b_Ctor, id, struct_type.getName()));
			struct_type.openScope();
			return;
		}

		FuncSTO f = new FuncSTO(id, struct_type, m_symtab.getLevel()+1);
		f.setStruct_member(true);
		struct_type.insert(f);
		struct_type.openScope();
		struct_type.setFunc(f);
		//DoParameters(params);
	}


	void DoDefaultConstructor(String id){
		if(struct_type == null){
			return;
		}
		else{
			FuncSTO f = new FuncSTO(id, struct_type, m_symtab.getLevel()+1);
			f.setStruct_member(true);
			Vector<STO> s = new Vector<STO>();
			f.setParam(s);
			struct_type.insert(f);
			struct_type.addConstructor(f);
			m_acg.writeMethodHeader(id + "." + id, "void");
			//m_acg.writeComment("Store Parameter");
			m_acg.writeParam("%i0", 68);
			m_acg.writeAssembly("\n");
			m_acg.writeEndFunction(id + "." + id + ".void", "" + 0);
			//acg add constructor
		}
	}
	void DoDefaultDestructor(String id){
		if(!struct_type.getHasDtor()){
			String callid = "~" + id;
			FuncSTO f = new FuncSTO(callid, struct_type, m_symtab.getLevel()+ 1);
			f.setStruct_member(true);
			struct_type.insert(f);
			struct_type.setDtor();
			m_acg.writeMethodHeader(id + ".$" + id, "void");
			//m_acg.writeComment("Store Parameter");
			m_acg.writeParam("%i0", 68);
			m_acg.writeAssembly("\n");
			String name =id+".$"+id+".void";
			m_acg.writeEndFunction(name, "" + 0);
		}
	}

	void DoDestructorDef(String id){
		String callid = "~" + id;
		if(!(struct_type.getName().equals(id))){
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error13b_Dtor, callid, struct_type.getName()));
			struct_type.openScope();
			return;
		}
		else{
			FuncSTO f = new FuncSTO(callid, struct_type, m_symtab.getLevel()+1);
			f.setStruct_member(true);
			struct_type.insert(f);
			struct_type.openScope();
			struct_type.setFunc(f);
			struct_type.setDtor();
			m_acg.writeMethodHeader(id + ".$" + id, "void");
			//m_acg.writeComment("Store Parameter");
			m_acg.writeParam("%i0", 68);
			m_acg.writeAssembly("\n");
			this.methodName=id+".$"+id+".void";
			//DoParameters(params);
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoBlockOpen()
	{
		if(struct_type != null){
			struct_type.openScope();
		}
		else {
			m_symtab.openScope();
		}
	}


	// Added method for break and continue
	void DoLoopOpen()
	{
		if(struct_type != null){
			struct_type.setScopeIsLoop(true);
		}
		else {
			m_symtab.setScopeIsLoop(true);
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void DoBlockClose()
	{
		if(struct_type != null){
			struct_type.closeScope();
		}
		else {
			m_symtab.closeScope();
		}
		m_declared = false;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO DoAssignExpr(STO stoDes, Operator op, STO b)
	{
		if(b instanceof ErrorSTO) return b;
		if(stoDes instanceof ErrorSTO) return stoDes;

		if (!stoDes.isModLValue())
		{
			// Good place to do the assign checks
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error3a_Assign);
			return new ErrorSTO(stoDes.getName());
		}

		STO result = op.checkOperands(stoDes, b);

		if(result instanceof ErrorSTO)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error3b_Assign, b.getType().getName(), stoDes.getType().getName()));
		}

		return result;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO DoFuncCall(STO sto, Object params)
	{
		if (!sto.isFunc())
		{
			String id = null;
			if(sto.getDeference()) {
				id = sto.getFullName();
			}
			else{
				id = sto.getName();
			}
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.not_function, id));
			return new ErrorSTO(sto.getName());
		}

		Vector<STO> pp = (Vector<STO>) params;
		Vector<STO> p = new Vector<STO>();
		if(pp == null || pp.size() == 0){
			pp = new Vector<STO>();
		}
		else {
			for(int i = 0; i < pp.size(); i++){
				STO s = pp.get(i);
				STO v = new VarSTO(s.getName(), s.getType());
				if(s instanceof VarSTO){
					v.setIsModifiable(s.getIsModifiable());
					v.setIsAddressable(s.getIsAddressable());
					v.setRef(s.isRef());
					v.setRefSTO(s.getRefSTO());
					v.setOffset(s.getOffset());
					v.setBase(s.getBase());
					v.setDereference(s.getDeference());
					v.setAccessFromArray(s.isAccessFromArray());
					v.strucOffset = s.strucOffset;
					v.isPointer = s.isPointer;
					v.isExtern = s.isExtern;
					p.add(v);
				}
				else{
					p.add(s);
				}
			}
		}


		if(sto.isError()) return sto;

		FuncSTO funcSTO = new FuncSTO(sto.getName(), sto.getType(), 0);
		boolean overload = ((FuncSTO) sto).isOverloaded();
		boolean def = sto.getDeference();
		funcSTO.setParam(p);
		funcSTO.setStructType(((FuncSTO) sto).accessStructType());
		funcSTO.setFullName(sto.getFullName());
		funcSTO.setRefSTO(sto.getRefSTO());
		funcSTO.setOffset(sto.getOffset());
		funcSTO.setBase(sto.getBase());


		boolean flag = false;
		if(((FuncSTO)sto).getStruct_member()) {
			if (struct_type != null) {
				flag = struct_type.validFunction(funcSTO);
				List<FuncSTO> funList = struct_type.getFuncList();
				FuncSTO sf = (FuncSTO)struct_type.accessGlobal(funcSTO.getName());

				if (!flag) {
					if (funList.size() == 1) {

						int size = 0;
						int cpsize = 0;
						if (funList.get(0).getParamList() != null) {
							size = funList.get(0).getParamList().size();
						}
						if (p != null) {
							cpsize = p.size();
						}
						m_nNumErrors++;
						m_errors.print(Formatter.toString(ErrorMsg.error5n_Call, cpsize, size));
						return new ErrorSTO(sto.getName());
					}
					else if(sf.isOverloaded()){
						m_nNumErrors++;
						m_errors.print(Formatter.toString(ErrorMsg.error9_Illegal, sto.getName()));
						return new ErrorSTO(sto.getName());
					}
					else
					{
						if(sf == null) return new ErrorSTO("no func");
						if(sf.getParamList() == null) sf.setParam(new Vector<STO>());
						if(funcSTO.getParamList() == null) funcSTO.setParam(new Vector<STO>());
						if (funcSTO.getParamList().size() != sf.getParamList().size()) {
							m_nNumErrors++;
							m_errors.print(Formatter.toString(ErrorMsg.error5n_Call, p.size(), sf.getParamList().size()));
							return new ErrorSTO(sto.getName()); //Still need to be check
						}
						for (int i = 0; i < p.size(); i++) {
							Type t_expect = sf.getParamList().get(i).getType();
							STO s_expect = sf.getParamList().get(i);
							Type t_param = p.get(i).getType();
							if (!(s_expect.isRef())) {
								if (!(t_param.isAssignableTo(t_expect))) {
									m_nNumErrors++;
									m_errors.print(Formatter.toString(ErrorMsg.error5a_Call, t_param.getName(), s_expect.getName(), t_expect.getName()));
									return new ErrorSTO(sto.getName()); //Still need to be check
								}
							} else {
								if (!(t_param.isEquivalentTo(t_expect))) {
									m_nNumErrors++;
									m_errors.print(Formatter.toString(ErrorMsg.error5r_Call, t_param.getName(), s_expect.getName(), t_expect.getName()));
									return new ErrorSTO(sto.getName()); //Still need to be check
								}
								if (!(p.get(i).isModLValue())) {
									m_nNumErrors++;
									m_errors.print(Formatter.toString(ErrorMsg.error5c_Call, s_expect.getName(), t_param.getName()));
									return new ErrorSTO(sto.getName()); //Still need to be check
								}
							}
						}
					}
				}
				sto = sf;
				if(sf.isOverloaded()){
					for(int i = 0; i < funList.size(); i++){
						Vector<STO> fp = funcSTO.getParamList();
						Vector<STO> sp = funList.get(i).getParamList();
						if(funcSTO.getName().equals(funList.get(i).getName())){
							if(fp == null && sp == null){
								sto = funList.get(i);
								//return sto;
								break;
							}
							if(fp != null && sp != null){
								if(fp.size() == sp.size()){
									boolean match = true;
									for(int j = 0; j < fp.size(); j++){
										Type fpt = fp.get(j).getType();
										Type spt = sp.get(j).getType();
										if(!(fpt.isEquivalentTo(spt))){
											match = false;
										}
										else{
											fp.get(j).setRef(sp.get(j).isRef());
										}
									}
									if(match){
										sto = funList.get(i);
										//return sto;
										break;
									}
								}
							}
						}
					}

				}
				//Assembler Funccall
				m_acg.writeFunCall(funcSTO, def);
				return sto;
			}
			StructType t;
			if ((t = ((FuncSTO) sto).accessStructType()) != null) {
				if (t != null) {
					flag = t.validFunction(funcSTO);
					List<FuncSTO> funList = t.getFuncList();
					FuncSTO sf = (FuncSTO)t.accessGlobal(funcSTO.getName());
					if (!flag) {
						if (funList.size() == 1) {

							int size = 0;
							int cpsize = 0;
							if (funList.get(0).getParamList() != null) {
								size = funList.get(0).getParamList().size();
							}
							if (p != null) {
								cpsize = p.size();
							}
							m_nNumErrors++;
							m_errors.print(Formatter.toString(ErrorMsg.error5n_Call, cpsize, size));
							return new ErrorSTO(sto.getName());
						}
						else if(sf.isOverloaded()){
							m_nNumErrors++;
							m_errors.print(Formatter.toString(ErrorMsg.error9_Illegal, sto.getName()));
							return new ErrorSTO(sto.getName());
						}
						else {
							if(sf == null) return new ErrorSTO("no func");
							if(sf.getParamList() == null) sf.setParam(new Vector<STO>());
							if(funcSTO.getParamList() == null) funcSTO.setParam(new Vector<STO>());
							if (funcSTO.getParamList().size() != sf.getParamList().size()) {
								m_nNumErrors++;
								m_errors.print(Formatter.toString(ErrorMsg.error5n_Call, p.size(), sf.getParamList().size()));
								return new ErrorSTO(sto.getName()); //Still need to be check
							}
							for (int i = 0; i < p.size(); i++) {
								Type t_expect = sf.getParamList().get(i).getType();
								STO s_expect = sf.getParamList().get(i);
								Type t_param = p.get(i).getType();
								if (!(s_expect.isRef())) {
									if (!(t_param.isAssignableTo(t_expect))) {
										m_nNumErrors++;
										m_errors.print(Formatter.toString(ErrorMsg.error5a_Call, t_param.getName(), s_expect.getName(), t_expect.getName()));
										return new ErrorSTO(sto.getName()); //Still need to be check
									}
								}
								else {
									if (!(t_param.isEquivalentTo(t_expect))) {
										m_nNumErrors++;
										m_errors.print(Formatter.toString(ErrorMsg.error5r_Call, t_param.getName(), s_expect.getName(), t_expect.getName()));
										return new ErrorSTO(sto.getName()); //Still need to be check
									}

									if (!(p.get(i).isModLValue())) {
										m_nNumErrors++;
										m_errors.print(Formatter.toString(ErrorMsg.error5c_Call, s_expect.getName(), t_param.getName()));
										return new ErrorSTO(sto.getName()); //Still need to be check
									}
								}
							}
						}
					}
					sto = sf;
					if(sf.isOverloaded()){
						for(int i = 0; i < funList.size(); i++){
							Vector<STO> fp = funcSTO.getParamList();
							Vector<STO> sp = funList.get(i).getParamList();
							if(funcSTO.getName().equals(funList.get(i).getName())){
								if(fp == null && sp == null){
									sto = funList.get(i);
									//return sto;
									break;
								}
								if(fp != null && sp != null){
									if(fp.size() == sp.size()){
										boolean match = true;
										for(int j = 0; j < fp.size(); j++){
											Type fpt = fp.get(j).getType();
											Type spt = sp.get(j).getType();
											if(!(fpt.isEquivalentTo(spt))){
												match = false;
											}
											else{
												fp.get(j).setRef(sp.get(j).isRef());
											}
										}
										if(match){
											sto = funList.get(i);
											//return sto;
											break;
										}
									}
								}
							}
						}

					}
				}
			}
			//Assembler Funccall
			m_acg.writeFunCall(funcSTO, def);
			return sto;
		}

		//Check 9 finish all overloading func

		else {
			int k = 0;
			String fullname = "";
			if(p==null){
				fullname = "void";
			}
			else{
				if(p.size() == 0){
					fullname = "void";
				}
				else{
					for(int i = 0; i < p.size(); i++){
						STO s = p.get(i);
						String temp = s.getType().getName();
						fullname = fullname + "." + temp;
					}
				}
			}
			funcSTO.setAssemblyName(fullname);
			if(overload) {

				while (k < declaredFunctions.size()) {
					FuncSTO f = declaredFunctions.get(k);
					flag = false;
					if (f.getName().equals(funcSTO.getName())) {
						if (p != null && f.getParamList() != null && f.getParamList().size() == p.size()) {
							boolean flag2 = true;  //check the types of params
							for (int j = 0; j < f.getParamList().size(); j++) {
								Type ftype = f.getParamList().get(j).getType();
								Type fdtype = p.get(j).getType();
								p.get(j).setRef(f.getParamList().get(j).isRef());
								if (!(ftype.isEquivalentTo(fdtype))) {
									flag2 = false;
								}
								else{
									f.getParamList().get(j).setRef(p.get(j).isRef());
								}
							}

							//find a match function
							if (flag2) {
								flag = true;
								funcSTO.setReturnType(f.getType());
								funcSTO.setRef(f.getRef());
								break;
							}
						}
						else {
							flag = false;
						}
					}
					k++;
				}
				if (!flag) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error9_Illegal, sto.getName()));
					return new ErrorSTO(sto.getName());
				}
				// Do Assembly instruction for function call here
				offset-=funcSTO.getType().getSize();
				funcSTO.setOffset(""+offset);
				funcSTO.setBase("%fp");
				funcSTO.isExtern = sto.isExtern;
				m_acg.writeFunCall(funcSTO, def);
				sto = funcSTO;
			}
			else {
				FuncSTO f = (FuncSTO) m_symtab.access(funcSTO.getName());
				if(f == null) return new ErrorSTO("no func");
				if(f.getParamList() == null) f.setParam(new Vector<STO>());
				if(funcSTO.getParamList() == null) funcSTO.setParam(new Vector<STO>());
				if (funcSTO.getParamList().size() != f.getParamList().size()) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error5n_Call, p.size(), f.getParamList().size()));
					return new ErrorSTO(sto.getName()); //Still need to be check
				}
				for (int i = 0; i < p.size(); i++) {
					Type t_expect = f.getParamList().get(i).getType();
					STO s_expect = f.getParamList().get(i);
					Type t_param = p.get(i).getType();
					if (!(s_expect.isRef())) {
						if (!(t_param.isAssignableTo(t_expect))) {
							m_nNumErrors++;
							m_errors.print(Formatter.toString(ErrorMsg.error5a_Call, t_param.getName(), s_expect.getName(), t_expect.getName()));
							return new ErrorSTO(sto.getName()); //Still need to be check
						}
					} else {
						if (!(t_param.isEquivalentTo(t_expect))) {
							m_nNumErrors++;
							m_errors.print(Formatter.toString(ErrorMsg.error5r_Call, t_param.getName(), s_expect.getName(), t_expect.getName()));
							return new ErrorSTO(sto.getName()); //Still need to be check
						}
						if (!(p.get(i).isModLValue())) {
							m_nNumErrors++;
							m_errors.print(Formatter.toString(ErrorMsg.error5c_Call, s_expect.getName(), t_param.getName()));
							return new ErrorSTO(sto.getName()); //Still need to be check
						}
					}
					funcSTO.getParamList().get(i).setRef(f.getParamList().get(i).isRef());
				}

				// Do Assembly instruction for function call here
				offset-=funcSTO.getType().getSize();
				funcSTO.setOffset(""+offset);
				funcSTO.setBase("%fp");
				funcSTO.isExtern = sto.isExtern;
				m_acg.writeFunCall(funcSTO, def);
				sto = funcSTO;
			}

		}



		return sto;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO DoDesignator2_Dot(STO sto, String strID, boolean arrow)
	{

		//Check struct
		String stoName = sto.getName();
		STO ss;
		if(sto.isError()) return sto;
		Type test;
		if(sto.getType() instanceof PointerType){
			PointerType pp = (PointerType) sto.getType();
			test = pp.getElementType();
		}
		else{
			test = sto.getType();
		}


		if(!(test.isStruct())){
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error14t_StructExp, sto.getType().getName()));
			return new ErrorSTO(sto.getName());
		}

		StructType t = (StructType) test;
		if(t.accessGlobal(strID) == null){
			if(!sto.isThis()) {
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error14f_StructExp, strID, t.getName()));
				return new ErrorSTO(sto.getName());
			}
			else{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error14c_StructExpThis, strID));
				return new ErrorSTO(sto.getName());
			}
		}
		else{
			ss = t.accessGlobal(strID);
			if(ss.isFunc()){
				FuncSTO f = (FuncSTO) ss;
				if(!ss.isThis()){
					f.setStructType(t);
					ss = f;
				}
			}
		}
		ss.setDereference(true);
		String fullname;
		if(arrow){
			fullname = "*" + stoName + "." + strID;
			sto.isPointer = true;
		}
		else{
			fullname = stoName + "." + strID;
		}

		ss.setRefSTO(sto);
		ss.setFullName(fullname);
		if(!(ss.isFunc())){
			if(ss.getType() instanceof ArrayType){
				offset -= 4; //Pointer to the array
			}
			else {
				offset -= ss.getType().getSize();
			}
			if(arrow){
				m_acg.writeArrowAccess(ss, offset);
				offset -= 4; //allocate pointer
			}
			else {
				m_acg.writeDotAccess(ss, offset);
			}
		}
		else{
			if(arrow){
				offset-=4; //Allocate for func pointer
			}
		}
		//if(ss.getRefSTO().isAccessFromArray()||ss.getType() instanceof ArrayType || ss.getRefSTO().isThis()){
		ss.setOffset("" + offset);
		//}
		ss.setBase("%fp");
		return ss;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO DoDesignator2_Array(STO a, STO b)
	{
		// Good place to do the array checks
		if(a instanceof ErrorSTO) return a;
		if(b instanceof ErrorSTO) return b;

		ArrayAccessOp op = new ArrayAccessOp("[]");
		STO result = op.checkOperands(a, b);

		if(result instanceof ErrorSTO)
		{
			if(result.getName().equals("indexOutOfBound"))
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error11b_ArrExp, ((ConstSTO)b).getIntValue(), ((ArrayType)a.getType()).getDimension()));
			}
			else if(result.getName().equals("indexNotInteger"))
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error11i_ArrExp, b.getType().getName()));
			}
			else if(result.getName().equals("notArrayOrPointerType"))
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error11t_ArrExp, a.getType().getName()));
			}
			else // if nullptr
			{
				m_nNumErrors++;
				m_errors.print(ErrorMsg.error15_Nullptr);
			}
		}
		return result;
	}

	//----------------------------------------------------------------
	//	This function is for searching variables(local first and then global)
	//----------------------------------------------------------------
	STO DoDesignator3_ID(String strID)
	{
		STO sto;
		if(struct_type != null){
			if(struct_type.getFunc() != null) {
				if ((struct_type.accessLocal(strID)) == null && m_symtab.access(strID) == null) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID));
					sto = new ErrorSTO(strID);
					return sto;
				}
				else if((sto = struct_type.accessLocal(strID)) != null){
					return sto;
				}
				else{
					sto = m_symtab.access(strID);
					return sto;
				}
			}
		}
		if ((sto = m_symtab.access(strID)) == null) {
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID));
			sto = new ErrorSTO(strID);
		}
		return sto;
	}

	// Added function when ::T_ID. This will only search the global variables
	STO DoDesignator3_ID_GLOBAL(String strID)
	{
		STO sto;

		if ((sto = m_symtab.accessGlobal(strID)) == null) {
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error0g_Scope, strID));
			sto = new ErrorSTO(strID);
		}

		return sto;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	Type DoStructType_ID(String strID)
	{
		STO sto;
		if(struct_type != null){
			String struct_name = struct_type.getName();
			if((sto=m_symtab.access(strID)) != null){
				if(sto.getType().isStruct()){
					return sto.getType();
				}
			}
			if(!(strID.equals(struct_name))){
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID));
				return new ErrorType();
			}
			else{
				return struct_type;
			}
		}

		if ((sto = m_symtab.access(strID)) == null)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID));
			return new ErrorType();
		}

		if (!sto.isStructdef()) {
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.not_type, sto.getName()));
			return new ErrorType();
		}
		return sto.getType();
	}

	STO DoThisSTO(){
		if(struct_type != null) {
			ThisSTO t = new ThisSTO("this", struct_type);
			t.setBase("%fp");
			t.setOffset("68");
			return t;
		} else {
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error14c_StructExpThis, "this"));
			return new ErrorSTO("this");
		}
	}

	// Added function: compareType
	STO DoBinaryExpr(STO a, Operator op, STO b)
	{
		// checking if a or b is ErrorSTO
		if(b instanceof ErrorSTO) return b;
		if(a instanceof ErrorSTO) return a;

		// get the result STO(it should has type in it)
		STO result = op.checkOperands(a,b);

		if(result instanceof ErrorSTO)
		{
			if(op instanceof ArithmeticOp)
			{
				if (op.getOpName().equals("%")) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error1w_Expr, result.getName(), op.getOpName(), "int"));
				} else {
					if(result.getName().equals("DivideByZero")) {
						m_nNumErrors++;
						m_errors.print(ErrorMsg.error8_Arithmetic);
					}
					else {
						m_nNumErrors++;
						m_errors.print(Formatter.toString(ErrorMsg.error1n_Expr, result.getName(), op.getOpName()));
					}
				}
			}
			else if(op instanceof ComparisonOp)
			{
				if(op instanceof EqualOp || op instanceof NotEqualOp) {
					if(result.getName().equals("OperandsTypeNotEqual"))
					{
						m_nNumErrors++;
						m_errors.print(Formatter.toString(ErrorMsg.error17_Expr,op.getOpName(), a.getType().getName(), b.getType().getName()));
					}
					else {
						m_nNumErrors++;
						m_errors.print(Formatter.toString(ErrorMsg.error1b_Expr, a.getType().getName(), op.getOpName(), b.getType().getName()));
					}
				}
				else {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error1n_Expr, result.getName(), op.getOpName()));
				}
			}
			else if(op instanceof BooleanOp)
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error1w_Expr, result.getName(), op.getOpName(), "bool"));
			}
			else if(op instanceof BitwiseOp)
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error1w_Expr, result.getName(), op.getOpName(), "int"));
			}
		}
		// the result should be R-Value which is nonAddress and nonModify
		result.setIsAddressable(false);
		result.setIsModifiable(false);
		return result;
	}

	// Added method
	STO DoUnaryOp(STO a, Operator op)
	{
		if(a instanceof ErrorSTO) return a;
		STO result = op.checkOperands(a);

		if(result instanceof ErrorSTO)
		{
			if(op instanceof NotOp) {
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error1u_Expr, result.getName(), op.getOpName(), "bool"));
			}
			else // otherwise PreOp or PostOp
			{
				m_nNumErrors++;
				if(result.getName().equals("TypeIncorrect"))
				{
					m_errors.print(Formatter.toString(ErrorMsg.error2_Type, a.getType().getName(), op.getOpName()));
				}
				else
				{
					m_errors.print(Formatter.toString(ErrorMsg.error2_Lval, op.getOpName()));
				}
			}
			return result;
		}

		result.setIsAddressable(false);
		result.setIsAddressable(false);
		return result;
	}

	//Added Method  *check if this is correct
	STO DoConditionalStmt(STO e, boolean isIf)
	{
		if (e instanceof ErrorSTO) return e;

		/*Type eType = e.getType();
		if( !(eType instanceof BoolType))
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error4_Test, eType.getName()));
			return new ErrorSTO(e.getName());
		}*/

		if(isIf)
		{
			m_acg.writeIfstmt(e);
		}
		else // while loop
		{
			m_acg.writeWhile(e);
		}

		return e;
	}

	//Added Method
	void DoReturnStmt(STO expr){
		if(expr != null && expr.isError()) return;
		FuncSTO func = m_symtab.getFunc();
		if(func == null) return;
		func.setReturn(true);
		Type ftype = func.getReturnType();

		if(expr == null){
			if(!(ftype.isVoid())) {
				m_nNumErrors++;
				m_errors.print(ErrorMsg.error6a_Return_expr);
				return;
			}
			else{
				m_acg.writeReturnNoRef(this.methodName, null);
				return;
			}
		}
		else{
			Type etype = expr.getType();
			if(func.getRef()){
				if(!(etype.isEquivalentTo(ftype))){
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error6b_Return_equiv, etype.getName(), ftype.getName()));
					return;
				}
				if(!(expr.isModLValue())){
					m_nNumErrors++;
					m_errors.print(ErrorMsg.error6b_Return_modlval);
					return;
				}
			}
			else{
				if(!(etype.isAssignableTo(ftype))) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error6a_Return_type, etype.getName(), ftype.getName()));
					return;
				}
			}
			if(func.getRef()) {
				m_acg.writeReturnRef(this.methodName, expr);
			}
			else{
				m_acg.writeReturnNoRef(this.methodName, expr);
			}
		}
	}

	void DoExitStmt(STO expr){
		if(expr.isError()) return;
		IntType t = new IntType();
		if(!(expr.getType().isAssignableTo(t))){
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error7_Exit,
					expr.getType().getName()));
			return;
		}
		m_acg.writeExit(expr);
	}

	void IsLoopBreak()
	{
		if(!m_symtab.getScopeIsLoop())
		{
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error12_Break);
		}

		m_acg.writeComment("break");
		m_acg.writeBreak();
	}

	void isLoopContinue()
	{
		if(!m_symtab.getScopeIsLoop())
		{
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error12_Continue);
		}

		m_acg.writeComment("continue");
		m_acg.writeContinue();
	}

	STO DoArrowAccess(STO des, String id)
	{
		if(des instanceof ErrorSTO) return des;

		ArrowAccessOp arrow = new ArrowAccessOp("->");
		STO result = arrow.checkOperands(des, id);

		if(result instanceof ErrorSTO)
		{
			if(result.getName().equals("nullptrAccess"))
			{
				m_nNumErrors++;
				m_errors.print(ErrorMsg.error15_Nullptr);
			}
			else if(result.getName().equals("notStructType"))
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error15_ReceiverArrow, des.getType().getName()));
			}
			// if id is an error
		}
		else
		{
			STO structResult = DoDesignator2_Dot(des, id, true);
			return structResult;
		}

		return result;
	}

	STO DoAddressOf(STO des)
	{
		if(des instanceof ErrorSTO) return des;

		AddressOfOp op = new AddressOfOp("&");
		STO result = op.checkOperands(des);

		if(result instanceof ErrorSTO)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error18_AddressOf, des.getType().getName()));
		}
		return result;
	}

	STO DoSizeOfSTO(STO a)
	{
		if(a instanceof ErrorSTO) return a;

		if(!a.getIsAddressable())
		{
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error19_Sizeof);
			return new ErrorSTO("variableNotAddressable");
		}

		ConstSTO result = new ConstSTO(a.getName(), new IntType(), a.getType().getSize());
		return result;
	}

	STO DoSizeOfPrimitive(Type t, Vector<STO> index)
	{
		STO arrayResult;
		if(index != null)
		{
			arrayResult = getArraySize(index);
			return arrayResult;
		}
		else if(t instanceof BasicType || t instanceof PointerType)
		{
			return new ConstSTO(t.getName(), new IntType(), 4);
		}
		else
		{
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error19_Sizeof);
			return new ErrorSTO("primitiveNoSize");
		}
	}

	// private helper method for array access
	private STO getArraySize(Vector<STO> index)
	{
		// first pass, checking if it has error
		for(int i = 0; i < index.size(); i++)
		{
			if(index.get(i) instanceof ErrorSTO)
			{
				return new ErrorSTO("indexHasError");
			}
		}

		for(int i = 0; i < index.size(); i++)
		{
			STO currentIndex = index.get(i);
			if(!(currentIndex.getType() instanceof IntType))
			{
				m_nNumErrors++;
				m_errors.print(Formatter.toString(ErrorMsg.error10i_Array, currentIndex.getType().getName()));
				return new ErrorSTO("indexHasError");
			}
			else if(!(currentIndex instanceof ConstSTO))
			{
				m_nNumErrors++;
				m_errors.print(ErrorMsg.error10c_Array);
				return new ErrorSTO("indexHasError");
			}
			else // when index is ConstSTO with IntType
			{
				if(!(((ConstSTO) currentIndex).getIntValue() > 0)) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO) currentIndex).getIntValue()));
					return new ErrorSTO("indexHasError");
				}
			}
		}

		int sizeOfArray = 4;
		for(int i = 0; i < index.size(); i++)
		{
			STO curIndex = index.get(i);
			sizeOfArray *= ((ConstSTO)curIndex).getIntValue();
		}
		return new ConstSTO(index.get(0).getName(), new IntType() ,sizeOfArray);
	}

	STO DoTypeCast(Type t, STO des)
	{
		if(des instanceof ErrorSTO) return des;

		TypeCastOp op = new TypeCastOp("("+t.getName() + ")");
		STO result = op.checkOperands(t, des);

		if(result instanceof ErrorSTO)
		{
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error20_Cast, des.getType().getName(), t.getName()));
		}
		else
		{
			// setting result to be R-value
			result.setIsModifiable(false);
			result.setIsAddressable(false);
		}

		return result;
	}

	void DoNewStmt(STO sto, Object o){
		if(!(sto.isModLValue())){
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error16_New_var);
			return;
		}
		if(!(sto.getType() instanceof PointerType)){
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error16_New, sto.getType().getName()));
			return;
		}
		else{
			PointerType pt = (PointerType) sto.getType();
			Type t = pt.getElementType();
				if (!(t.isStruct())) {
					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error16b_NonStructCtorCall, pt.getName()));
					return;
				}
				else if(t.isStruct()){
					//The object is a pointer to struct
					Vector<STO> p;
					if(o != null) {
						p = (Vector<STO>) o;
					}
					else{
						p = new Vector<STO>();
					}
					FuncSTO f = new FuncSTO(sto.getName(), t, 0);
					f.setOffset(sto.getOffset());
					f.setBase(sto.getBase());
					f.setParam(p);
					m_acg.writeNewStmt(sto);
					offset-=4;//allocate space for constructor pointer
					m_acg.writeNewStmtStruct(sto, f, offset);
				}
			else{
				m_acg.writeNewStmt(sto);
			}
		}

	}
	void DoDeleteStmt(STO sto){
		if(!(sto.isModLValue())){
			m_nNumErrors++;
			m_errors.print(ErrorMsg.error16_Delete_var);
			return;
		}
		if(!(sto.getType() instanceof PointerType)){
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.error16_Delete, sto.getType().getName()));
			return;
		}
		PointerType p = (PointerType) sto.getType();
		if(p.getElementType() instanceof StructType){
			offset -=4; // allocate for destructor pointer
		}
		m_acg.writeDeleteStmt(sto, offset);

	}

	// might need this later for AS
	void StoreCurrentFunc(String id, Vector<STO> p)
	{
		String fullname = "";
		if(struct_type!=null) {
			id=struct_type.getName() + "." + id;
		}

		if(p==null){
			fullname = "void";
		}
		else{
			if(p.size() == 0){
				fullname = "void";
			}
			else{
				for(int i = 0; i < p.size(); i++){
					STO s = p.get(i);
					String temp = s.getType().getName();
					if(s.getType() instanceof PointerType){
						PointerType pp = (PointerType) s.getType();
						temp = pp.getElementType().getName() + "$";
					}
					else if(s.getType() instanceof ArrayType){
						ArrayType aa = (ArrayType) s.getType();
						int dim = aa.getDimension();
						temp = "$"+dim+"$";
						while(aa.getElementType() instanceof ArrayType){
							ArrayType atmp = (ArrayType) aa.getElementType();
							dim = atmp.getDimension();
							temp = temp + "$"+dim+"$";
							aa = atmp;
						}
						temp = aa.getElementType().getName() + temp;
					}

					if(fullname.equals("")){
						fullname = temp;
					}
					else {
						fullname = fullname + "." + temp;
					}
				}
			}
		}
		this.methodName = id+"."+fullname;
		if(struct_type==null) {
			if(m_declared){
				m_acg.writeMethodHeader(id, fullname);
			}
			else{
				m_acg.writeGlobalMethodHeader(id, fullname);
			}
		}
		else{
			m_acg.writeMethodHeader(id, fullname);
		}
	}

	void resetOffset()
	{
		this.offset = 0; // resetting the offset
	}

	private void initUnknownVar(boolean isStatic, String id, Type t, STO opInit)
	{
		STO var = m_symtab.access(id);
		STO initVar = opInit;
		m_acg.writeComment(id + " = " + opInit.getName());
		m_acg.initUnknownVar(var, initVar, false);
	}

	private void initConstValue(boolean isStatic, String id, STO opInit)
	{
		int value = ((ConstSTO) opInit).getIntValue();
		STO localVar = m_symtab.access(id);
		m_acg.writeComment(id + " = " + value);
		m_acg.initConst(localVar, "" + value);
	}

	private void initUnknownFloat(boolean isStatic, String id, Type t, STO opInit)
	{
		STO var = m_symtab.access(id);
		m_acg.writeComment(id + " = " + opInit.getName());
		m_acg.initUnknownVar(var, opInit, true);
	}

	private void initUnknownInt2Float(boolean isStatic, String id, Type t, STO opInit)
	{
		STO var = m_symtab.access(id);
		m_acg.writeComment(id + " = " + opInit.getName());
		this.offset -= 4;
		m_acg.initInt2Float(var, opInit, "" + this.offset);
	}

	private void initFloatConst(boolean isStatic, String id, STO opInit)
	{
		float value = ((ConstSTO) opInit).getFloatValue();
		STO var = m_symtab.access(id);
		m_acg.writeComment(id + " = " + value);
		m_acg.initFloat(var, "" + value);
	}

	private void initExprSTOFloat(boolean isStatic, String id, Type t, STO opInit) {
		if(m_symtab.isGlobal()) {
			//float value = ((ConstSTO) opInit).getFloatValue();
			//m_acg.initGlobalBasic(id, "" + value, true, isStatic);
		}
		else // local scope
		{
			STO localVar = m_symtab.access(id);
			m_acg.writeComment(id + " = " + opInit.getName());
			m_acg.initLocalExprFloat(localVar, opInit);
		}
	}

	/********* */

	void DoCOUT(STO e){
		if(e != null) {
			m_acg.writePrint(e);
		}
	}

	public void DoElse()
	{
		int elseCount = elseCounterStack.peek();
		m_acg.writeElse(elseCount);
	}

	public void DoEndIf()
	{
		int elseCount = elseCounterStack.pop();
		m_acg.writeEndIf(elseCount);
	}

	public void DoCin(STO input){
		m_acg.writeCin(input);
	}

	public void DoShortCircuit(STO LHS, String opName)
	{
		m_acg.writeLHS(LHS, opName);
	}
}

/**
 * Created by Dennis on 4/16/2015.
 */
class ArrayAccessOp extends CompositeOp {
    public ArrayAccessOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a, STO b)
    {
        Type aType = a.getType();
        Type bType = b.getType();

        if(aType instanceof ArrayType)
        {
            if(b instanceof ConstSTO)
            {
                if(bType instanceof IntType)
                {
                    if(((ConstSTO) b).getIntValue() < ((ArrayType) aType).getDimension() && ((ConstSTO) b).getIntValue() >= 0) {
                        VarSTO result = new VarSTO(a.getName() + "[" + ((ConstSTO) b).getIntValue() + "]", ((ArrayType) aType).getElementType());
                        /*if(((ArrayType) aType).getElementType() instanceof StructType){
                            MyParser.offset -= 4; //doing pointers only
                        }
                        else {*/
                            MyParser.offset -= ((ArrayType) aType).getElementType().getSize();
                        //}
                        result.setOffset(""+MyParser.offset);
                        result.setBase("%fp");
                        result.setAccessFromArray(true);
                        result.setRefSTO(a.getRefSTO());
                        MyParser.m_acg.writeComment(a.getName() + "[" + ((ConstSTO) b).getIntValue() + "]");
                        MyParser.m_acg.writeArrayAccess(a, b, result.getOffset());
                        // unset the modifiable flag
                        if(result.getType() instanceof ArrayType)
                            result.setIsModifiable(false);
                        return result;
                    }
                    else // out of bound
                    {
                        return new ErrorSTO("indexOutOfBound");
                    }
                }
                else
                {
                    return new ErrorSTO("indexNotInteger");
                }
            }
            else // if it is other STO excepts ErrorSTO(will be check in runtime)
            {
                if(bType instanceof IntType) {
                    VarSTO result = new VarSTO(a.getName() + "[" + b.getName() + "]", ((ArrayType) aType).getElementType());
                    MyParser.offset -= ((ArrayType) aType).getElementType().getSize();
                    result.setOffset(""+MyParser.offset);
                    result.setBase("%fp");
                    result.setAccessFromArray(true);
                    result.setRefSTO(a.getRefSTO());
                    MyParser.m_acg.writeComment(a.getName() + "[" + b.getName() + "]");
                    MyParser.m_acg.writeArrayAccess(a, b, result.getOffset());
                    // unset the modifiable flag
                    if(result.getType() instanceof ArrayType)
                        result.setIsModifiable(false);
                    return result;
                }
                else
                {
                    return new ErrorSTO("indexNotInteger");
                }
            }
        }
        else if(aType instanceof PointerType)
        {
            if(aType instanceof NullPointerType)
            {
                return new ErrorSTO("nullptrAccess");
            }
            else if(bType instanceof IntType)
            {
                VarSTO result = new VarSTO(a.getName() + "[" + b.getName() + "]", ((PointerType) aType).getElementType());
                MyParser.offset -= ((PointerType) aType).getElementType().getSize();
                result.setOffset(""+MyParser.offset);
                result.setBase("%fp");
                result.setAccessFromArray(true);
                result.setRefSTO(a.getRefSTO());
                MyParser.m_acg.writeComment(a.getName() + "[" + b.getName() + "]");
                MyParser.m_acg.writeArrayAccessPointer(a, b, result.getOffset());
                return result;
            }
            else
            {
                return new ErrorSTO("indexNotInteger");
            }
        }
        else
        {
            return new ErrorSTO("notArrayOrPointerType");
        }
    }
}

/**
 * Created by Dennis on 4/6/2015.
 */
abstract class Operator {
    private String opName;

    public Operator(String operatorName)
    {
        opName = operatorName;
    }

    public STO checkOperands(STO a, STO b)
    {
        return null;
    }

    public STO checkOperands(STO a) { return null; }

    public STO checkOperands(Type t, STO a) {return null;}

    public STO checkOperands(STO des, String id) {return null;}

    public STO checkOperands(STO a, STO b, AssemblyCodeGenerator acg) {return null;}
    public String getOpName()
    {
        return opName;
    }
}

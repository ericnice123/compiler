/**
 * Created by Dennis on 4/16/2015.
 */
class PointerType extends CompositeType {
    Type elementType;

    public PointerType()
    {
        super("pointerType", 4);
    }

    public PointerType(String name, int size)
    {
        super(name, size);
    }

    public void setName(String name)
    {
        super.setName(name);
    }

    public Type getElementType()
    {
        return elementType;
    }

    public void setElementType(Type t)
    {
        elementType = t;
    }

    public boolean isAssignableTo(Type t)
    {
        if(t instanceof PointerType)
        {
            Type aType = ((PointerType) t).getElementType();
            Type bType = elementType;
            while(true)
            {
                if(!(aType instanceof PointerType) || !(bType instanceof PointerType))
                {
                    if(aType.isEquivalentTo(bType))
                        return true;
                    else
                        return false;
                }
                else
                {
                    aType = ((PointerType) aType).getElementType();
                    bType = ((PointerType) bType).getElementType();
                }
            }
        }
        else
        {
            return false;
        }
    }

    public boolean isEquivalentTo(Type t)
    {
        if(t instanceof PointerType)
        {
            Type aType = ((PointerType) t).getElementType();
            Type bType = elementType;
            while(true)
            {
                if(!(aType instanceof PointerType) || !(bType instanceof PointerType))
                {
                    if(aType.isEquivalentTo(bType))
                        return true;
                    else
                        return false;
                }
                else
                {
                    aType = ((PointerType) aType).getElementType();
                    bType = ((PointerType) bType).getElementType();
                }
            }
        }
        else
        {
            return false;
        }
    }
}

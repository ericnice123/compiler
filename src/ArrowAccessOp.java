/**
 * Created by Tsung-Huan on 4/22/15.
 */
class ArrowAccessOp extends CompositeOp {
    public ArrowAccessOp(String opName)
    {
        super(opName);
    }


    // probably need to pass in String id to check the element in struct
    public STO checkOperands(STO des, String id)
    {
        Type aType = des.getType();

        if(aType instanceof NullPointerType)
        {
            return new ErrorSTO("nullptrAccess");
        }
        else if(!(aType instanceof PointerType))
        {
            return new ErrorSTO("notStructType");
        }
        else // if aType is struct type
        {
            Type element = ((PointerType) aType).getElementType();
            if(!(element instanceof StructType))
            {
                return new ErrorSTO("notStructType");
            }

            STO result = ((StructType)element).access(id);
            if(result == null)
            {
                return new ErrorSTO("elementNotFound");
            }
            // this should return struct's element
            return result;
        }
    }
}

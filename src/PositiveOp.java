/**
 * Created by Dennis on 4/15/2015.
 */
class PositiveOp extends UnaryOp {
    public PositiveOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a)
    {
        Type aType = a.getType();

        if(!(aType instanceof NumericType)) {
            return new ErrorSTO("OperandNotNumeric");
        }
        else
        {
            if(a instanceof ConstSTO)
            {
                if(aType instanceof IntType) {
                    int result = ((ConstSTO) a).getIntValue();
                    return new ConstSTO(""+result, new IntType(), result);
                }
                else {
                    float result = ((ConstSTO) a).getFloatValue();
                    return new ConstSTO(""+result, new FloatType(), result);
                }
            }
            else {
                if(aType instanceof IntType) {
                    ExprSTO exprSTO = new ExprSTO("+" + a.getName(), new IntType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment("+" + a.getName());
                    MyParser.m_acg.writeUnaryOp(a, "" + MyParser.offset, "+");
                    return exprSTO;
                }
                else {
                    ExprSTO exprSTO = new ExprSTO("+" + a.getName(), new FloatType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment("+" + a.getName());
                    MyParser.m_acg.writeUnaryOpFloat(a, "" + MyParser.offset, "+");
                    return exprSTO;
                }
            }
        }
    }
}

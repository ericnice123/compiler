/**
 * Created by Dennis on 4/20/2015.
 */
class StarDerefOp extends PointerOp {
    public StarDerefOp(String opName)
    {
        super(opName);
    }


    public STO checkOperands(STO a)
    {
        // in what case we need to use nullptr?
        Type aType = a.getType();
        if(aType instanceof NullPointerType)
        {
            return new ErrorSTO("nullptr");
        }
        else if(!(aType instanceof PointerType))
        {
            return new ErrorSTO("notPointerType");
        }
        else
        {
            Type resultType = ((PointerType) aType).getElementType();
            VarSTO resultVar = new VarSTO(resultType.getName(), resultType);
            resultVar.setAccessFromArray(true);
            MyParser.offset -= 4;
            resultVar.setOffset(""+MyParser.offset);
            resultVar.setBase("%fp");
            resultVar.setIsAddressable(true);
            resultVar.setIsModifiable(true);
            MyParser.m_acg.writeComment("*"+a.getName());
            MyParser.m_acg.writePtrDere(a, resultVar);
            return resultVar;
        }
    }
}

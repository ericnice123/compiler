//---------------------------------------------------------------------
// CSE 131 Reduced-C Compiler Project
// Copyright (C) 2008-2015 Garo Bournoutian and Rick Ord
// University of California, San Diego
//---------------------------------------------------------------------

class VarSTO extends STO
{
	private boolean reference = false;
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public VarSTO(String strName)
	{
		super(strName);
		// You may want to change the isModifiable and isAddressable
		// fields as necessary
		super.setIsAddressable(true);
		super.setIsModifiable(true);
	}

	public VarSTO(String strName, Type typ)
	{
		super(strName, typ);
		// You may want to change the isModifiable and isAddressable
		// fields as necessary
		if(typ instanceof ArrayType)
		{
			// if ArrayType set id to be non-modifiable lvalue
			super.setIsAddressable(true);
			super.setIsModifiable(false);
		}
		else if(typ instanceof NullPointerType)
		{
			super.setIsModifiable(false);
			super.setIsAddressable(false);
		}
		else {
			super.setIsAddressable(true);
			super.setIsModifiable(true);
		}
	}

	public void setOffset(String offset)
	{
		super.setOffset(offset);
	}

	public String getOffset()
	{
		return super.getOffset();
	}

	public void setBase(String base)
	{
		super.setBase(base);
	}

	public String getBase()
	{
		return super.getBase();
	}
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean isVar() 
	{
		return true;
	}

	public void setRef(boolean ref) { reference = ref; }
	public boolean isRef(){return reference;}
}

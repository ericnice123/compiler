/**
 * Created by Tsung-Huan on 4/9/15.
 */
class GreaterThanOp extends ComparisonOp {

    public GreaterThanOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a, STO b)
    {
        Type aType = a.getType();
        Type bType = b.getType();

        if(!(aType instanceof NumericType) || !(bType instanceof NumericType))
        {
            if(!(aType instanceof NumericType))
                return new ErrorSTO(aType.getName());
            else    // if bType isn't NumericType
                return new ErrorSTO(bType.getName());
        }
        else
        {
            if(a instanceof ConstSTO && b instanceof ConstSTO)
            {
                int result = ((ConstSTO) a).getValue().compareTo(((ConstSTO) b).getValue());
                if(result == 1)
                {
                    return new ConstSTO("true", new BoolType(), 1);
                }
                else
                {
                    return new ConstSTO("false", new BoolType(), 0);
                }
            }
            else
            {
                if(aType instanceof IntType && bType instanceof IntType) {
                    ExprSTO exprSTO = new ExprSTO(a.getName() + " > " + b.getName(), new BoolType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + " > " + b.getName());
                    MyParser.m_acg.writeRelation(a, b, "" + MyParser.offset, ">");
                    return exprSTO;
                }
                else // float and int compare
                {
                    ExprSTO exprSTO = new ExprSTO(a.getName() + " > " + b.getName(), new BoolType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + " > " + b.getName());
                    MyParser.m_acg.writeRelationFloat(a, b, "" + MyParser.offset, ">");
                    return exprSTO;
                }
            }
        }
    }
}

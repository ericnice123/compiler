//---------------------------------------------------------------------
// CSE 131 Reduced-C Compiler Project
// Copyright (C) 2008-2015 Garo Bournoutian and Rick Ord
// University of California, San Diego
//---------------------------------------------------------------------

import java.util.*;

class SymbolTable
{
	private Stack<Scope> m_stkScopes;
	private int m_nLevel;
	private Scope m_scopeGlobal;
	private FuncSTO m_func = null;
    
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public SymbolTable()
	{
		m_nLevel = 0;
		m_stkScopes = new Stack<Scope>();
		m_scopeGlobal = null;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void insert(STO sto)
	{
		Scope scope = m_stkScopes.peek();
		scope.InsertLocal(sto);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO accessGlobal(String strName)
	{
		return m_scopeGlobal.access(strName);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO accessLocal(String strName)
	{
		Scope scope = m_stkScopes.peek();
		return scope.accessLocal(strName);
	}

	//----------------------------------------------------------------
	// Changed on 4-4 by Oscar
	// Not fully tested yet, recheck when int type is stored.
	//----------------------------------------------------------------
	public STO access(String strName)
	{
		Stack<Scope> stk = new Stack();
		Scope scope;
		STO stoReturn = null;

		for (Enumeration<Scope> e = m_stkScopes.elements(); e.hasMoreElements();)
		{
			scope = e.nextElement();
			stk.push(scope);
		}

		// fixed, originally there's a bug when the id is not defined, it will still pop the empty stk
		// which caused an error
		while (!stk.empty()) {
			scope = stk.pop();
			if ((stoReturn = scope.access(strName)) != null) {
				return stoReturn;
			}
		}

		return null;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void openScope()
	{
		Scope scope = new Scope();
		// The first scope created will be the global scope.
		if (m_scopeGlobal == null) {
			m_scopeGlobal = scope;
			m_stkScopes.push(scope);
			m_nLevel++;
		}
		else // added code for break and continue
		{
			Scope s = m_stkScopes.peek();
			if(s.getIsLoop()) {
				scope.setIsLoop(true);
			}
			m_stkScopes.push(scope);
			m_nLevel++;
		}

	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void closeScope()
	{
		m_stkScopes.pop();
		m_nLevel--;
	}

	// added method for break and continue
	public void setScopeIsLoop(boolean b)
	{
		Scope s = m_stkScopes.peek();
		s.setIsLoop(b);
	}

	// added method for break and continue
	public boolean getScopeIsLoop()
	{
		Scope s = m_stkScopes.peek();
		return s.getIsLoop();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public int getLevel()
	{
		return m_nLevel;
	}

	public boolean isGlobal(){
		return m_scopeGlobal==m_stkScopes.peek();
	}

	//----------------------------------------------------------------
	//	This is the function currently being parsed.
	//----------------------------------------------------------------
	public FuncSTO getFunc() { return m_func; }
	public void setFunc(FuncSTO sto) { m_func = sto; }
}

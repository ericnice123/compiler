/**
 * Created by Tsung-Huan on 4/6/15.
 */
class BoolType extends BasicType {
    public BoolType()
    {
        super("bool", 4); // don't know if it is bits represents or byte, im treating as bits for now
    }

    public boolean isAssignableTo(Type t)
    {
        if(t instanceof BoolType)
            return true;
        else
            return false;
    }

    public boolean isEquivalentTo(Type t) {
        if(t instanceof BoolType)
            return true;
        else
            return false;
    }
}

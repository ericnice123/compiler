/**
 * Created by Tsung-Huan on 4/9/15.
 */
class BwOrOp extends BitwiseOp {
    public BwOrOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a, STO b)
    {
        Type aType = a.getType();
        Type bType = b.getType();

        if(aType instanceof IntType && bType instanceof IntType)
        {
            if(a instanceof ConstSTO && b instanceof ConstSTO)
            {
                int result = ((ConstSTO) a).getIntValue() | ((ConstSTO) b).getIntValue();
                return new ConstSTO(""+result, new IntType(), result);
            }
            else
            {
                ExprSTO exprSTO = new ExprSTO(a.getName() + " | " +b.getName(), new IntType());
                MyParser.offset -= 4;
                exprSTO.setOffset(""+MyParser.offset);
                exprSTO.setBase("%fp");
                MyParser.m_acg.writeComment(a.getName() + " | " + b.getName());
                MyParser.m_acg.LLCS(a, b, "" + MyParser.offset, "|");
                return exprSTO;
            }
        }
        else
        {
            if(!(aType instanceof IntType))
                return new ErrorSTO(aType.getName());
            else
                return new ErrorSTO(bType.getName());
        }
    }
}

/**
 * Created by Dennis on 4/10/2015.
 */
class AssignOp extends AssignmentOp {
    public AssignOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a, STO b)
    {
        Type aType = a.getType();
        Type bType = b.getType();


        if(bType.isAssignableTo(aType)) {
            ExprSTO exprSTO = new ExprSTO(a.getName() + " = " + b.getName(), a.getType());
            exprSTO.setOffset(a.getOffset());
            exprSTO.setBase(a.getBase());

            MyParser.m_acg.writeComment(a.getName() + " = " + b.getName());
            MyParser.m_acg.writeAssign(a, b);
            exprSTO.setRefSTO(a.getRefSTO());

            return exprSTO;
        }
        else {
            return new ErrorSTO("not assignable");
        }
    }
}

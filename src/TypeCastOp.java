import com.sun.org.apache.xpath.internal.operations.Bool;

import java.math.BigDecimal;

/**
 * Created by Tsung-Huan on 4/23/15.
 */
class TypeCastOp extends CompositeOp{
    public TypeCastOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(Type t, STO a)
    {
        // first check t type
        if(t instanceof BasicType || t instanceof PointerType)
        {
            if(t instanceof NullPointerType)
                return new ErrorSTO("CastTypeError");
        }
        else
        {
            return new ErrorSTO("CastTypeError");
        }

        // t type ok, then check a type
        Type aType = a.getType();

        if(aType instanceof BasicType || aType instanceof PointerType)
        {
            if(aType instanceof NullPointerType)
            {
                return new ErrorSTO("CastTypeError");
            }
            else // ok to cast
            {
                if(a instanceof ConstSTO)
                {
                    if(t instanceof IntType)
                    {
                        BigDecimal value = ((ConstSTO) a).getValue();
                        //System.out.print("curValue = " + value);
                        int v = value.intValue();
                        //System.out.println("castValue = " + v);
                        return new ConstSTO(t.getName(), t, v);
                    }
                    else if(t instanceof FloatType)
                    {
                        BigDecimal value = ((ConstSTO) a).getValue();
                        //System.out.print("curValue = " + value);
                        float v = value.floatValue();
                        //System.out.println("castValue = " + v);
                        return new ConstSTO(t.getName(), t, v);
                    }
                    else if(t instanceof BoolType)
                    {
                        BigDecimal value = ((ConstSTO) a).getValue();
                        int v = value.intValue();
                        if(v == 0)
                            return new ConstSTO(t.getName(), t, 0);
                        else
                            return new ConstSTO(t.getName(), t, 1);
                    }
                    else // if PointerType
                    {
                        ExprSTO result = new ExprSTO("("+t.getName()+")"+a.getName(), t);
                        MyParser.offset -= 4;
                        result.setOffset(""+MyParser.offset);
                        result.setBase("%fp");
                        MyParser.m_acg.writeComment(result.getName());
                        MyParser.m_acg.writeConstToPtr(a, result);
                        return result;
                    }
                }
                else // if a not constSTO  (Var(whatever type) to whatever)
                {
                    ExprSTO result = new ExprSTO("("+t.getName()+")"+a.getName(), t);
                    MyParser.offset -= 4;
                    result.setOffset(""+MyParser.offset);
                    result.setBase("%fp");
                    MyParser.m_acg.writeComment(result.getName());
                    MyParser.m_acg.writeVarToPtr(a, result);
                    return result;
                }
            }
        }
        else
        {
            return new ErrorSTO("CastTypeError");
        }
    }
}


import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Stack;
import java.util.Vector;

/**
 * An Assembly Code Generator example emphasizing well thought design and
 * the use of java 1.5 constructs.
 *
 * Disclaimer : This is not code meant for you to use directly but rather
 * an example from which I hope you can learn useful constructs and
 * conventions.
 *
 * Topics of importance (Corrosponding to the inline comment numbers below)
 *
 * 1) We will use this variable to denote the current level of indentation.
 *    For assembly there is usually only one or two levels of nesting.
 *
 * 2) A collection of static final error messages.  This isn't so important in
 *    your project but is useful.  The static keyword means we only make 3 
 *    strings, shared across potentially multiple AssemblyCodeGenerator(s).
 *    It is Java convention to spell constant variables with upper casing.
 *
 * 3) FileWriter is a basic IO class which can write basic types such as
 *    Strings to a file.  For performance you may want to look into the
 *    BufferedWriter class.
 *
 * 4) This is a template for our file header.  It is very basic consisting only
 *    of a time stamp.
 *
 * 5) This is the string we will use as an indentation seperator.  We are 
 *    encapsulating this seperator into one variable so we only need to change
 *    the initialization if we want to change our spacing to say 4 spaces for
 *    example.  Imagine if you simply used the literal "\t" in 500 places and
 *    then you decide you want to change it to 4 spaces!  Aside from regular 
 *    expressions you have a lot of work ahead of you.
 *
 * 6) These are constant String templates that will be used for code
 *    generation.  It is nice to isolate these in one place or even in another
 *    file so that we can quickly make universal changes if needed.  You will
 *    notice that we could generate an entirely different language by simply
 *    changing the construct definitions.  I recommend defining all operations
 *    as well as formats.  Operations are things like add, mul, set, etc.
 *    Formats are like {OPERATION} {REG_1}, {REG_2}, {REG_3} etc.
 *
 * 7) Here we are making a call to writeAssembly to write our header with the
 *    current time.  writeAssembly explained later.
 *
 * 8) These methods are used to increase or decrease our current indentation
 *    level.  You might ask why make a method for a simple inc/dec?  We are
 *    encapsulating the notion of adjusting indentation.  It just so happens
 *    that this current implementation is just a variable increment or 
 *    decrement, but who is to say that the operation won't be more advanced
 *    in the future.  Maybe we want to log a message everytime we increment
 *    or decrement indentation.  We wouldn't want to add the logging code
 *    everywhere we were incrementing the variable (if we didn't have the
 *    methods).
 *
 * 9) This signature may look foreign to you.  What is says is that we have 
 *    public method named writeAssembly which takes as parameters a String
 *    followed by 1 or more strings.  This construct is called "VarArgs" and
 *    is a Java 1.5 feature.  This allows you to write one method which can
 *    be applied to any number of parameters.  This method simply takes in 
 *    a template and all the strings that will be substituted into the 
 *    template.  When you are actually in the method, the parameter 
 *    String ... params will be an array of strings.
 *
 * 10) This is where we use our indent_level.  We will indent indent_level levels
 *     of indentation.  That is an awkward sentence isn't it!  StringBuilder is
 *     an efficient class to build strings from concatentations.  If your 
 *     concatenations span multiple lines of code, using a StringBuilder can
 *     offer signifigant performance when compared to using the + operator.
 *     This topic can get fairly detailed, send me an email or come talk to me
 *     in the lab for more details.
 *
 * 11) Here we are writing the message to file, notice we are using the 
 *     String.format method which takes a printf like format string followed
 *     by an array of Objects which are the parameters to the format string.
 *
 * 12) Main is just a small demo that will create a tiny assembly file in the
 *     current directory called "rc.s".  This file doesn't compile and is
 *     not meant to.
 *
 * @author Evan Worley
 */

public class AssemblyCodeGenerator {
    // 1
    private int indent_level = 0;
    public static int const_count = 0;
    public static int st_count = 0;
    public static Vector<STO> struct_count = new Vector<STO>();
    public static Vector<STO> global_static_struct = new Vector<STO>();

    // 2
    private static final String ERROR_IO_CLOSE =
            "Unable to close fileWriter";
    private static final String ERROR_IO_CONSTRUCT =
            "Unable to construct FileWriter for file %s";
    private static final String ERROR_IO_WRITE =
            "Unable to write to fileWriter";

    // 3
    private FileWriter fileWriter;

    // 4
    private static final String FILE_HEADER =
            "/*\n" +
                    " * Generated %s\n" +
                    " */\n\n";

    // 5
    private static final String SEPARATOR = "\t";

    // 6
    // section string
    public static final String DATA_SEC = ".section" + SEPARATOR + "\".data\"\n";
    public static final String BSS_SEC = ".section" + SEPARATOR + "\".bss\"\n";
    public static final String TEXT_SEC = ".section" + SEPARATOR + "\".text\"\n";
    public static final String INIT_SEC = ".section" + SEPARATOR + "\".init\"\n";
    public static final String RODATA_SEC = ".section" + SEPARATOR + "\".rodata\"\n";
    public static final String ALIGN = ".align" + SEPARATOR + "4\n";
    public static final String SKIP = ".skip" + SEPARATOR + "%s\n";

    // global string
    public static final String GLOBALVARDECL = ".global" + SEPARATOR + "%s\n";
    public static final String GLOBALVAR = "%s:\n";

    // assign string
    public static final String WORD = ".word" + SEPARATOR + "%s\n";
    public static final String SINGLE = ".single" + SEPARATOR + "0r%s\n";

    // operator strings
    public static final String SET_OP = "set\t";
    public static final String SAVE_OP = "save";
    public static final String MOV_OP = "mov\t";
    public static final String ADD_OP = "add";
    public static final String SUB_OP = "sub";
    public static final String NEG = "neg\t";
    public static final String LD_OP = "ld" + SEPARATOR + "[%s], %s\n";
    public static final String ST_OP = "st" + SEPARATOR + "%s, [%s]\n";
    public static final String CALL_OP = "call" + SEPARATOR + "%s\n";
    public static final String NOP = "nop\n";
    public static final String INC = "inc" + SEPARATOR + "%s\n";

    // floating point op
    public static final String FITOS = "fitos";
    public static final String FADDS = "fadds";

    // comment
    public static final String COMMENT = "! %s\n";

    // formatter
    public static final String TWO_PARAM = "%s" + SEPARATOR + "%s, %s\n";
    public static final String THREE_PARAM = "%s" + SEPARATOR + "%s, %s, %s\n";

    // function
    public static final String GLOBALINITFUNC = ".$.init.%s:\n";
    public static final String GLOBALINITFUNCFINI = ".$.init.%s.fini:\n";
    public static final String ENDFUNC = "END of function ";
    public static final String RET = "ret\n";
    public static final String RESTORE = "restore\n";
    public static final String SAVESPACE = "SAVE.%s = -(92 + %s) & -8\n";
    public static final String PRINTBOOL = ".$$.printBool";
    public static final String BOOLSTR = ".$$.strTF";
    public static final String ARRCHECK = ".$$.arrCheck";
    public static final String PTRCHECK = ".$$.ptrCheck";

    // local variable
    public static final String LOCALVARDECL = "%s:\n";

    // branches
    public static final String BNE = "bne\t" + SEPARATOR + "%s\n";
    public static final String FBNE = "fbne" + SEPARATOR + "%s\n";
    public static final String BLE = "ble\t" + SEPARATOR + "%s\n";
    public static final String FBLE = "fble" + SEPARATOR + "%s\n";
    public static final String BL = "bl\t" + SEPARATOR + "%s\n";
    public static final String FBL = "fbl\t" + SEPARATOR + "%s\n";
    public static final String BG = "bg\t" + SEPARATOR + "%s\n";
    public static final String FBG = "fbg\t" + SEPARATOR + "%s\n";
    public static final String BGE = "bge\t" + SEPARATOR + "%s\n";
    public static final String FBGE = "fbge" + SEPARATOR + "%s\n";
    public static final String BE = "be\t" + SEPARATOR + "%s\n";
    public static final String FBE = "fbe\t" + SEPARATOR + "%s\n";
    public static final String BA = "ba\t" + SEPARATOR + "%s\n";


    public AssemblyCodeGenerator(String fileToWrite) {
        try {
            fileWriter = new FileWriter(fileToWrite);

            // 7
            writeAssembly(FILE_HEADER, (new Date()).toString());
            writeAssembly("\n");
            increaseIndent();
            writeAssembly("\n");
            writeAssembly(".section\t\".rodata\"\n");
            writeAssembly(".align\t4\n");
            decreaseIndent();
            writeAssembly(".$$.intFmt:\n");
            writeAssembly("\t.asciz\t\"%%d\"\n");
            writeAssembly(".$$.strFmt:\n");
            writeAssembly("\t.asciz\t\"%%s\"\n");
            writeAssembly(".$$.strTF:\n");
            writeAssembly("\t.asciz\t\"false\\0\\0\\0true\"\n");
            writeAssembly(".$$.strEndl:\n");
            writeAssembly("\t.asciz\t\"\\n\"\n");
            writeAssembly(".$$.strArrBound:\n");
            writeAssembly("\t.asciz\t\"Index Value of %%d is outside legal range[0, %%d).\\n\"\n");
            writeAssembly(".$$.strNullPtr:\n");
            writeAssembly("\t.asciz\t\"Attempt to dereference NULL pointer.\\n\"\n\n");

            increaseIndent();
            writeAssembly(TEXT_SEC);
            writeAssembly(ALIGN);
            decreaseIndent();


            writeAssembly(PRINTBOOL + ":\n");
            increaseIndent();
            writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
            writeAssembly(TWO_PARAM, SET_OP, BOOLSTR, "%o0");
            writeAssembly(TWO_PARAM, "cmp\t", "%g0", "%i0");
            writeAssembly("be" + SEPARATOR + ".$$.printBool2\n");
            writeAssembly(NOP);
            writeAssembly(THREE_PARAM, ADD_OP, "%o0", "8", "%o0");
            decreaseIndent();
            writeAssembly(".$$.printBool2:\n");
            increaseIndent();
            writeAssembly("call" + SEPARATOR + "printf\n");
            writeAssembly(NOP);
            writeAssembly(RET);
            writeAssembly(RESTORE);
            decreaseIndent();
            writeAssembly("\n");

            writeAssembly(ARRCHECK + ":\n");
            increaseIndent();
            writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
            writeAssembly(TWO_PARAM, "cmp\t", "%i0", "%g0");
            writeAssembly("bl" + SEPARATOR + ".$$.arrCheck2\n");
            writeAssembly(NOP);
            writeAssembly(TWO_PARAM, "cmp\t", "%i0", "%i1");
            writeAssembly("bge" + SEPARATOR + ".$$.arrCheck2\n");
            writeAssembly(NOP);
            writeAssembly(RET);
            writeAssembly(RESTORE);
            decreaseIndent();
            writeAssembly(".$$.arrCheck2:\n");
            increaseIndent();
            writeAssembly(TWO_PARAM, SET_OP, ".$$.strArrBound", "%o0");
            writeAssembly(TWO_PARAM, "mov", "%i0", "%o1");
            writeAssembly(CALL_OP, "printf");
            writeAssembly(TWO_PARAM, "mov", "%i1", "%o2");
            writeAssembly(CALL_OP, "exit");
            writeAssembly(TWO_PARAM, "mov", "1", "%o0");
            writeAssembly(RET);
            writeAssembly(RESTORE);
            decreaseIndent();
            writeAssembly("\n");
            writeAssembly(".$$.ptrCheck:\n");
            increaseIndent();
            writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
            writeAssembly(TWO_PARAM, "cmp\t", "%i0", "%g0");
            writeAssembly("bne" + SEPARATOR + ".$$.ptrCheck2\n");
            writeAssembly(NOP);
            writeAssembly(TWO_PARAM, SET_OP, ".$$.strNullPtr", "%o0");
            writeAssembly(NOP);
            writeAssembly(CALL_OP, "printf");
            writeAssembly(TWO_PARAM, "mov", "%i1", "%o2");
            writeAssembly(CALL_OP, "exit");
            writeAssembly(TWO_PARAM, "mov", "1", "%o0");
            decreaseIndent();
            writeAssembly(".$$.ptrCheck2:\n");
            increaseIndent();
            writeAssembly(RET);
            writeAssembly(RESTORE);
            decreaseIndent();
            writeAssembly("\n");

        } catch (IOException e) {
            System.err.printf(ERROR_IO_CONSTRUCT, fileToWrite);
            e.printStackTrace();
            System.exit(1);
        }
    }


    // 8
    public void decreaseIndent() {
        indent_level--;
    }

    public void dispose() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            System.err.println(ERROR_IO_CLOSE);
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void increaseIndent() {
        indent_level++;
    }



    // 9
    public void writeAssembly(String template, String ... params) {
        StringBuilder asStmt = new StringBuilder();

        // 10
        for (int i=0; i < indent_level; i++) {
            asStmt.append(SEPARATOR);
        }

        // 11
        asStmt.append(String.format(template, (Object[]) params));

        try {
            fileWriter.write(asStmt.toString());
        } catch (IOException e) {
            System.err.println(ERROR_IO_WRITE);
            e.printStackTrace();
        }
    }


    /*
     *  some headers or comments
     */
    public void writeComment(String comment)
    {
        writeAssembly(COMMENT, comment);
    }

    public void writeRodataSec()
    {
        writeAssembly("\n");
        writeAssembly(RODATA_SEC);
        writeAssembly(ALIGN);
    }

    public void writeTextSection()
    {
        writeAssembly("\n"); // new line just to make code clean
        writeAssembly(TEXT_SEC);
        writeAssembly(ALIGN);
        writeAssembly("\n");
    }

    public void writeInitSection(String initName)
    {
        increaseIndent();
        writeAssembly("\n");
        writeAssembly(INIT_SEC);
        writeAssembly(ALIGN, "4");
        writeAssembly(CALL_OP, initName);
        writeAssembly(NOP);
        decreaseIndent();
    }

    public void writeGlobalMethodHeader(String methodName, String returnType)
    {
        increaseIndent();
        writeAssembly(GLOBALVARDECL, methodName);
        decreaseIndent();
        writeAssembly(GLOBALVAR, methodName);
        writeAssembly(GLOBALVAR, methodName + "." + returnType);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, "SAVE." + methodName + "." + returnType, "%g1");
        writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "%g1", "%sp");
        increaseIndent();
        writeAssembly("\n");
    }
    public void writeMethodHeader(String methodName, String returnType)
    {
        writeAssembly(GLOBALVAR, methodName + "." + returnType);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, "SAVE." + methodName + "." + returnType, "%g1");
        writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "%g1", "%sp");
        increaseIndent();
        writeAssembly("\n");
    }

    public void writeEndFunction(String methodName, String saveValue)
    {
        decreaseIndent();
        writeAssembly(COMMENT, "End of function " + methodName + ".fini");
        writeAssembly(CALL_OP, methodName + ".fini");
        writeAssembly(NOP);
        writeAssembly(RET);
        writeAssembly(RESTORE);
        writeAssembly(SAVESPACE, methodName, saveValue);
        decreaseIndent();
        writeDtor(methodName);
    }

    public void writeDtor(String methodName)
    {
        boolean end = false;
        writeAssembly("\n");
        writeAssembly(GLOBALVAR, methodName + ".fini");
        increaseIndent();
        writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
        if(struct_count.size()!=0) {
            int totalStruct = struct_count.size();
            while(totalStruct > 0){
                STO s = struct_count.get(totalStruct-1);
                String name = s.getName();
                String dtorname = s.getFullName();
                if(s.isStatic){
                    decreaseIndent();
                    writeAssembly(dtorname + ".fini:\n");
                    increaseIndent();
                    writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
                }
                writeAssembly(TWO_PARAM, SET_OP, dtorname, "%o0");
                writeAssembly(LD_OP, "%o0", "%o0");
                writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
                writeAssembly(BE, dtorname+".fini.skip");
                writeAssembly(NOP);
                writeAssembly(CALL_OP, name + ".$"+ name + ".void");
                writeAssembly(NOP);
                writeAssembly(TWO_PARAM, SET_OP, dtorname, "%o0");
                writeAssembly(ST_OP, "%g0", "%o0");
                decreaseIndent();
                writeAssembly(dtorname+".fini.skip:\n");
                increaseIndent();
                if(end = s.isStatic){
                    writeAssembly(RET);
                    writeAssembly(RESTORE);
                    writeAssembly("\n");
                    writeAssembly(".section\t\".fini\"\n");
                    writeAssembly(ALIGN);
                    writeAssembly(CALL_OP, dtorname + ".fini");
                    writeAssembly(NOP);
                    writeAssembly("\n");
                    writeAssembly(TEXT_SEC);
                    writeAssembly(ALIGN);
                }
                else{
                    if(totalStruct>1) {
                        s = struct_count.get(totalStruct - 2);
                        if (s.isStatic) {
                            writeAssembly(RET);
                            writeAssembly(RESTORE);
                        }
                    }
                }
                totalStruct--;
            }
        }

        if(!end) {
            writeAssembly(RET);
            writeAssembly(RESTORE);
        }
        struct_count = new Vector<STO>();
        decreaseIndent();
    }

    public void writeGSDtor(){
        if(global_static_struct.size()!=0) {
            int totalStruct = global_static_struct.size();
            while(totalStruct > 0) {
                STO s = global_static_struct.get(totalStruct - 1);
                String name = s.getName();
                String dtorname = s.getFullName();
                decreaseIndent();
                writeAssembly(dtorname + ".fini:\n");
                increaseIndent();
                writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
                writeAssembly(TWO_PARAM, SET_OP, dtorname, "%o0");
                writeAssembly(LD_OP, "%o0", "%o0");
                writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
                writeAssembly(BE, dtorname + ".fini.skip");
                writeAssembly(NOP);
                writeAssembly(CALL_OP, name + ".$" + name + ".void");
                writeAssembly(NOP);
                writeAssembly(TWO_PARAM, SET_OP, dtorname, "%o0");
                writeAssembly(ST_OP, "%g0", "%o0");
                decreaseIndent();
                writeAssembly(dtorname + ".fini.skip:\n");
                increaseIndent();

                writeAssembly(RET);
                writeAssembly(RESTORE);
                writeAssembly("\n");
                writeAssembly(".section\t\".fini\"\n");
                writeAssembly(ALIGN);
                writeAssembly(CALL_OP, dtorname + ".fini");
                writeAssembly(NOP);
                writeAssembly("\n");
                writeAssembly(TEXT_SEC);
                writeAssembly(ALIGN);

                totalStruct--;
            }
        }


        global_static_struct = new Vector<STO>();
        decreaseIndent();
    }

    public void writeReturnNoRef(String funName, STO rSTO){
       if(rSTO == null){
           writeComment("return;");
           writeAssembly(CALL_OP, funName + ".fini");
           writeAssembly(NOP);
           writeAssembly(RET);
           writeAssembly(RESTORE);
       }
       else{
           if(rSTO instanceof ConstSTO){
               if(rSTO.getType() instanceof FloatType) {
                   const_count++;
                   writeAssembly(RODATA_SEC);
                   writeAssembly(ALIGN);
                   decreaseIndent();
                   String temp = ".$$.float." + const_count;
                   writeAssembly(temp + ":\n");
                   increaseIndent();
                   writeAssembly(".single" + SEPARATOR + "0r" + rSTO.getName());
                   writeAssembly("\n");
                   writeAssembly(TEXT_SEC);
                   writeAssembly(ALIGN);
                   writeAssembly(TWO_PARAM, SET_OP, temp, "%l7");
                   writeAssembly(LD_OP, "%l7", "%f0");
               }
               else if (rSTO.getType() instanceof BoolType){
                   String bool;
                   if(rSTO.getName().equals("true")){
                       bool = "1";
                   }
                   else{
                       bool = "0";
                   }
                   writeAssembly(TWO_PARAM, SET_OP, bool,"%i0");
               }
               else{
                   writeAssembly(TWO_PARAM, SET_OP, rSTO.getName(), "%i0");
               }
                   writeAssembly(CALL_OP, funName + ".fini");
                   writeAssembly(NOP);
                   writeAssembly(RET);
                   writeAssembly(RESTORE);
                   writeAssembly("\n");
           }
           else{
               String offset = rSTO.getOffset();
               writeComment("return " + rSTO.getName());
               writeAssembly(TWO_PARAM, SET_OP, offset, "%l7");
               writeAssembly(THREE_PARAM, ADD_OP, rSTO.getBase(), "%l7", "%l7");
               if(rSTO.isRef()){
                   writeAssembly(LD_OP, "%l7", "%l7");
               }
               if(rSTO.getType() instanceof FloatType){
                   writeAssembly(LD_OP, "%l7", "%f0");
               }
               else{
                   writeAssembly(LD_OP, "%l7", "%i0");
               }
               writeAssembly(CALL_OP, funName +".fini");
               writeAssembly(NOP);
               writeAssembly(RET);
               writeAssembly(RESTORE);
               writeAssembly("\n");
           }
       }
    }

    public void writeReturnRef(String funName, STO rSTO){
        if(rSTO == null){
            writeComment("return;");
            writeAssembly(CALL_OP, funName + ".fini");
            writeAssembly(NOP);
            writeAssembly(RET);
            writeAssembly(RESTORE);
        }
        else{
                String offset = rSTO.getOffset();
                writeComment("return " + rSTO.getName());
                writeAssembly(TWO_PARAM, SET_OP, offset, "%i0");
                writeAssembly(THREE_PARAM, ADD_OP, rSTO.getBase(), "%i0", "%i0");
                writeAssembly(LD_OP, "%i0", "%i0");
                writeAssembly(CALL_OP, funName +".fini");
                writeAssembly(NOP);
                writeAssembly(RET);
                writeAssembly(RESTORE);
                writeAssembly("\n");
        }
    }

    public void writeExit(STO eSTO){
        //set esto num %o0
        writeComment("exit(" + eSTO.getName() + ")");
        if(eSTO instanceof ConstSTO){
            writeAssembly(TWO_PARAM, SET_OP, eSTO.getName(), "%o0");
        }
        else{
            String offset = eSTO.getOffset();
            writeAssembly(TWO_PARAM, SET_OP, offset, "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, eSTO.getBase(), "%l7", "%l7");
            writeAssembly(LD_OP, "%l7", "%o0");
        }
        writeAssembly(CALL_OP, "exit");
        writeAssembly(NOP);
        writeAssembly("\n");

        //call exit
        //nop
    }

    public void LLCS(STO a, STO b, String lastOffset, String op)
    {
        // Load
        if(!(a instanceof ConstSTO))
        {
            writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
            if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o0");
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) a).getIntValue(), "%o0");
        }

        // Load
        if(!(b instanceof ConstSTO))
        {
            writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
            if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o1");
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) b).getIntValue(), "%o1");
        }

        // compute
        if(op.equals("+"))
        {
            writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o0");
        }
        if(op.equals("-"))
        {
            writeAssembly(THREE_PARAM, SUB_OP, "%o0", "%o1", "%o0");
        }
        if(op.equals("*"))
        {
            writeAssembly(CALL_OP, ".mul");
            writeAssembly(NOP);
            writeAssembly(TWO_PARAM, MOV_OP, "%o0", "%o0");
        }
        if(op.equals("/"))
        {
            writeAssembly(CALL_OP, ".div");
            writeAssembly(NOP);
            writeAssembly(TWO_PARAM, MOV_OP, "%o0", "%o0");
        }
        if(op.equals("%"))
        {
            writeAssembly(CALL_OP, ".rem");
            writeAssembly(NOP);
            writeAssembly(TWO_PARAM, MOV_OP, "%o0", "%o0");
        }
        if(op.equals("&"))
        {
            writeAssembly(THREE_PARAM, "and", "%o0", "%o1", "%o0");
        }
        if(op.equals("|"))
        {
            writeAssembly(THREE_PARAM, "or", "%o0", "%o1", "%o0");
        }
        if(op.equals("^"))
        {
            writeAssembly(THREE_PARAM, "xor", "%o0", "%o1", "%o0");
        }

        // lastOffset location
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void LLCSFloat(STO a, STO b, String lastOffset, String op)
    {
        Type aType = a.getType();
        Type bType = b.getType();
        if(a instanceof ConstSTO)
        {
            if(aType instanceof IntType)
            {
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) a).getIntValue(), "%o0");
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
            }
            else // float type
            {
                const_count++;
                writeRodataSec();
                decreaseIndent();
                writeAssembly(LOCALVARDECL, ".$$.float." + const_count);
                increaseIndent();
                writeAssembly(SINGLE, "" + ((ConstSTO) a).getFloatValue());
                writeTextSection();
                writeAssembly(TWO_PARAM, SET_OP, ".$$.float." + const_count, "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
            }
        }
        else // not ConstSTO
        {
            if(aType instanceof IntType)
            {
                writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
                if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o0");
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
            }
            else // float type
            {
                writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
                if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%f0");
            }
        }

        if(b instanceof ConstSTO)
        {
            if(bType instanceof IntType)
            {
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) b).getIntValue(), "%o0");
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f1");
                writeAssembly(TWO_PARAM, FITOS, "%f1", "%f1");
            }
            else // float type
            {
                const_count++;
                writeRodataSec();
                decreaseIndent();
                writeAssembly(LOCALVARDECL, ".$$.float." + const_count);
                increaseIndent();
                writeAssembly(SINGLE, "" + ((ConstSTO) b).getFloatValue());
                writeTextSection();
                writeAssembly(TWO_PARAM, SET_OP, ".$$.float." + const_count, "%l7");
                writeAssembly(LD_OP, "%l7", "%f1");
            }
        }
        else // not ConstSTO
        {
            if(bType instanceof IntType)
            {
                writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o0");
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f1");
                writeAssembly(TWO_PARAM, FITOS, "%f1", "%f1");
            }
            else // float type
            {
                writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%f1");
            }
        }

        if(op.equals("+")) {
            writeAssembly(THREE_PARAM, FADDS, "%f0", "%f1", "%f0");
        }
        if(op.equals("-"))
        {
            writeAssembly(THREE_PARAM, "fsubs", "%f0", "%f1", "%f0");
        }
        if(op.equals("*"))
        {
            writeAssembly(THREE_PARAM, "fmuls", "%f0", "%f1", "%f0");
        }
        if(op.equals("/"))
        {
            writeAssembly(THREE_PARAM, "fdivs", "%f0", "%f1", "%f0");
        }

        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%f0", "%o1");
        writeAssembly("\n");
    }

    public void writeIncOp(STO a, String lastOffset, String opName, boolean isPre) {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
        if(a.isAccessFromArray() || a.getRefSTO() != null || a.isPointer || a.isRef())
        {
            writeAssembly(LD_OP, "%l7", "%l7");
        }
        writeAssembly(LD_OP, "%l7", "%o0");
        if(a.getType() instanceof PointerType)
        {
            writeAssembly(TWO_PARAM, SET_OP, ""+a.getType().getSize(), "%o1");
        }
        else {
            writeAssembly(TWO_PARAM, SET_OP, "1", "%o1");
        }

        if (opName.equals("++")) {
            writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o2");
        }
        if(opName.equals("--"))
        {
            writeAssembly(THREE_PARAM, SUB_OP, "%o0", "%o1", "%o2");
        }

        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        if(isPre) {
            writeAssembly(ST_OP, "%o2", "%o1");
        }
        else {
            writeAssembly(ST_OP, "%o0", "%o1");
        }
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");
        if(a.isAccessFromArray() || a.getRefSTO() != null || a.isPointer || a.isRef())
        {
            writeAssembly(LD_OP, "%o1", "%o1");
        }
        writeAssembly(ST_OP, "%o2", "%o1");
        writeAssembly("\n");
    }

    public void writeIncOpFloat(STO a, String lastOffset, String opName, boolean isPre)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
        if(a.isAccessFromArray())
        {
            writeAssembly(LD_OP, "%l7", "%l7");
        }
        writeAssembly(LD_OP, "%l7", "%f0");
        writeRodataSec();
        decreaseIndent();
        const_count++;
        writeAssembly(LOCALVARDECL, ".$$.float."+const_count);
        increaseIndent();
        writeAssembly(SINGLE, "1.0");
        decreaseIndent();
        writeTextSection();
        writeAssembly(TWO_PARAM, SET_OP, ".$$.float."+const_count, "%l7");
        writeAssembly(LD_OP, "%l7", "%f1");

        if(opName.equals("++"))
        {
            writeAssembly(THREE_PARAM, "fadds", "%f0", "%f1", "%f2");
        }
        if(opName.equals("--"))
        {
            writeAssembly(THREE_PARAM, "fsubs", "%f0", "%f1", "%f2");
        }

        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        if(isPre) {
            writeAssembly(ST_OP, "%f2", "%o1");
        }
        else
        {
            writeAssembly(ST_OP, "%f0", "%o1");
        }

        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");
        if(a.isAccessFromArray())
        {
            writeAssembly(LD_OP, "%o1", "%o1");
        }
        writeAssembly(ST_OP, "%f2", "%o1");
        writeAssembly("\n");
    }

    public void writeRelation(STO a, STO b, String lastOffset, String opName)
    {
        // Load
        if(!(a.getType() instanceof NullPointerType)) {
            if (!(a instanceof ConstSTO)) {
                writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
                if (a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef()) {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o0");
            } else {
                writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) a).getIntValue(), "%o0");
            }
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, "0", "%o0");
        }

        // Load
        if(!(b.getType() instanceof NullPointerType)) {
            if (!(b instanceof ConstSTO)) {
                writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                if (b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef()) {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o1");
            } else {
                writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) b).getIntValue(), "%o1");
            }
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, "0", "%o1");
        }

        writeAssembly(TWO_PARAM, "cmp\t", "%o0", "%o1");

        MyParser.cmp_counter++;
        if(opName.equals(">"))
        {
            writeAssembly(BLE, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals(">="))
        {
            writeAssembly(BL, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("<="))
        {
            writeAssembly(BG, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("<"))
        {
            writeAssembly(BGE, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("=="))
        {
            writeAssembly(BNE, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("!="))
        {
            writeAssembly(BE, ".$$.cmp."+MyParser.cmp_counter);
        }

        writeAssembly(TWO_PARAM, MOV_OP, "%g0", "%o0");
        writeAssembly(INC, "%o0");
        decreaseIndent();
        writeAssembly(LOCALVARDECL, ".$$.cmp." + MyParser.cmp_counter);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void writeRelationFloat(STO a, STO b, String lastOffset, String opName)
    {
        Type aType = a.getType();
        Type bType = b.getType();
        if(a instanceof ConstSTO)
        {
            if(aType instanceof IntType)
            {
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) a).getIntValue(), "%o0");
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
            }
            else // float type
            {
                const_count++;
                writeRodataSec();
                decreaseIndent();
                writeAssembly(LOCALVARDECL, ".$$.float." + const_count);
                increaseIndent();
                writeAssembly(SINGLE, "" + ((ConstSTO) a).getFloatValue());
                writeTextSection();
                writeAssembly(TWO_PARAM, SET_OP, ".$$.float." + const_count, "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
            }
        }
        else // not ConstSTO
        {
            if(aType instanceof IntType)
            {
                writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
                if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o0");
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
            }
            else // float type
            {
                writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
                if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%f0");
            }
        }

        if(b instanceof ConstSTO)
        {
            if(bType instanceof IntType)
            {
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) b).getIntValue(), "%o0");
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f1");
                writeAssembly(TWO_PARAM, FITOS, "%f1", "%f1");
            }
            else // float type
            {
                const_count++;
                writeRodataSec();
                decreaseIndent();
                writeAssembly(LOCALVARDECL, ".$$.float." + const_count);
                increaseIndent();
                writeAssembly(SINGLE, "" + ((ConstSTO) b).getFloatValue());
                writeTextSection();
                writeAssembly(TWO_PARAM, SET_OP, ".$$.float." + const_count, "%l7");
                writeAssembly(LD_OP, "%l7", "%f1");
            }
        }
        else // not ConstSTO
        {
            if(bType instanceof IntType)
            {
                writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o0");
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f1");
                writeAssembly(TWO_PARAM, FITOS, "%f1", "%f1");
            }
            else // float type
            {
                writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%f1");
            }
        }

        writeAssembly(TWO_PARAM, "fcmps", "%f0", "%f1");
        writeAssembly(NOP);

        MyParser.cmp_counter++;
        if(opName.equals(">"))
        {
            writeAssembly(FBLE, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals(">="))
        {
            writeAssembly(FBL, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("<="))
        {
            writeAssembly(FBG, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("<"))
        {
            writeAssembly(FBGE, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("=="))
        {
            writeAssembly(FBNE, ".$$.cmp."+MyParser.cmp_counter);
        }
        if(opName.equals("!="))
        {
            writeAssembly(FBE, ".$$.cmp."+MyParser.cmp_counter);
        }

        writeAssembly(TWO_PARAM, MOV_OP, "%g0", "%o0");
        writeAssembly(INC, "%o0");
        decreaseIndent();
        writeAssembly(LOCALVARDECL, ".$$.cmp." + MyParser.cmp_counter);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void writeLHS(STO a, String opName)
    {
        MyParser.shortCircuitCounter++;
        writeComment("Short Circuit LHS");
        if(a instanceof ConstSTO)
        {
            writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) a).getIntValue(), "%o0");
        }
        else // not ConstSTO
        {
            writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
            if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o0");
            writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
            if(opName.equals("&&"))
            {
                writeAssembly(BE, ".$$.andorSkip." + MyParser.shortCircuitCounter);
            }
            if(opName.equals("||"))
            {
                writeAssembly(BNE, ".$$.andorSkip."+MyParser.shortCircuitCounter);
            }
            writeAssembly(NOP);
        }
    }

    public void writeBoolExpr(STO a, STO b, String lastOffset, String opName)
    {
        writeComment(a.getName() + " " + opName + " " + b.getName());
        writeAssembly("\n");
        writeComment("Short Circuit RHS");

        if(b instanceof ConstSTO)
        {
            writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) b).getIntValue(), "%o0");
        }
        else // not ConstSTO
        {
            writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
            if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o0");
        }

        writeAssembly(TWO_PARAM, "cmp\t", "%o0", "%g0");
        if(opName.equals("&&"))
        {
            writeAssembly(BE, ".$$.andorSkip." + MyParser.shortCircuitCounter);
        }
        if(opName.equals("||"))
        {
            writeAssembly(BNE, ".$$.andorSkip."+MyParser.shortCircuitCounter);
        }
        writeAssembly(NOP);

        writeAssembly(BA, ".$$.andorEnd."+MyParser.shortCircuitCounter);
        if(opName.equals("&&"))
        {
            writeAssembly(TWO_PARAM, MOV_OP, "1", "%o0");
        }
        if(opName.equals("||"))
        {
            writeAssembly(TWO_PARAM, MOV_OP, "0","%o0");
        }
        decreaseIndent();
        writeAssembly(LOCALVARDECL, ".$$.andorSkip." + MyParser.shortCircuitCounter);
        increaseIndent();
        if(opName.equals("&&"))
        {
            writeAssembly(TWO_PARAM, MOV_OP, "0", "%o0");
        }
        if(opName.equals("||"))
        {
            writeAssembly(TWO_PARAM, MOV_OP, "1","%o0");
        }
        decreaseIndent();
        writeAssembly(LOCALVARDECL, ".$$.andorEnd."+MyParser.shortCircuitCounter);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void writeNotOp(STO a, String lastOffset)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
        if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
        {
            writeAssembly(LD_OP, "%l7", "%l7");
        }
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(THREE_PARAM, "xor", "%o0", "1", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }


    /*
     *  if statement
     */
    public void writeIfstmt(STO a)
    {
        MyParser.else_counter++;
        int copy_else_counter = MyParser.else_counter;
        MyParser.elseCounterStack.push(copy_else_counter);
        if(a instanceof ConstSTO)
        {
            writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) a).getIntValue(), "%o0");
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
            if (a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o0");
        }

        writeAssembly(TWO_PARAM, "cmp\t", "%o0", "%g0");
        writeAssembly(BE, ".$$.else." + MyParser.else_counter);
        writeAssembly(NOP);
        writeAssembly("\n");
        increaseIndent();
    }

    public void writeElse(int elseCount)
    {
        writeAssembly(BA, ".$$.endif."+elseCount);
        writeAssembly(NOP);
        writeAssembly("\n");
        writeComment("else");
        decreaseIndent();
        writeAssembly(LOCALVARDECL, ".$$.else." + elseCount);
        increaseIndent();
        writeAssembly("\n");
    }

    public void writeEndIf(int elseCount)
    {
        decreaseIndent();
        writeComment("endif");
        writeAssembly(LOCALVARDECL, ".$$.endif." + elseCount);
        writeAssembly("\n");
    }

    /*
     * while statement
     */
    public void writeWhileHeader()
    {
        decreaseIndent();
        MyParser.whileCounter++;
        int copy_whileCounter = MyParser.whileCounter;
        MyParser.whileCounterStack.push(copy_whileCounter);
        writeAssembly(LOCALVARDECL, ".$$.loopCheck." + MyParser.whileCounter);
        increaseIndent();
        increaseIndent();
        writeAssembly("\n");
    }

    public void writeWhile(STO a)
    {
        writeComment("Check loop condition");
        if(a instanceof ConstSTO)
        {
            writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) a).getIntValue(), "%o0");
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
            if(a.isAccessFromArray() || a.isPointer || a.getRefSTO() != null || a.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o0");
        }
        writeAssembly(TWO_PARAM, "cmp\t", "%o0", "%g0");
        writeAssembly(BE, ".$$.loopEnd." + MyParser.whileCounterStack.peek());
        writeAssembly(NOP);
        writeAssembly("\n");
        writeComment("Start of loop body");
        increaseIndent();
    }

    public void writeWhileEnd()
    {
        writeComment("End of loop body");
        writeAssembly(BA, ".$$.loopCheck." + MyParser.whileCounterStack.peek());
        writeAssembly(NOP);
        decreaseIndent();
        writeAssembly(LOCALVARDECL, ".$$.loopEnd." + MyParser.whileCounterStack.pop());
        writeAssembly("\n");
    }

    /*
     *
     */
    public void writeForeach(STO iterVar, STO expr, String lastOffset)
    {
        writeComment("foreach(...)");
        writeComment("traversal ptr = --array");
        writeAssembly(TWO_PARAM, SET_OP, expr.getOffset(), "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, expr.getBase(), "%o0", "%o0");
        if(expr.getRefSTO() != null && expr.getRefSTO().isThis())
        {
            writeAssembly(LD_OP, "%o0", "%o0");
        }
        writeAssembly(TWO_PARAM, SET_OP, "" + ((ArrayType) expr.getType()).getElementType().getSize(), "%o1");
        writeAssembly(THREE_PARAM, SUB_OP, "%o0", "%o1", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        decreaseIndent();
        MyParser.whileCounter++;
        int whileCounter_copy = MyParser.whileCounter;
        MyParser.whileCounterStack.push(whileCounter_copy);
        writeAssembly(LOCALVARDECL, ".$$.loopCheck." + MyParser.whileCounter);
        increaseIndent();
        increaseIndent();
        writeComment("++traversal ptr");
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(LD_OP, "%o1", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, "" + ((ArrayType) expr.getType()).getElementType().getSize(), "%o2");
        writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o2", "%o0");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeComment("traversal ptr < array end addr?");
        writeAssembly(TWO_PARAM, SET_OP, expr.getOffset(), "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, expr.getBase(), "%o0", "%o0");
        if(expr.getRefSTO() != null && expr.getRefSTO().isThis())
        {
            writeAssembly(LD_OP, "%o0", "%o0");
        }
        writeAssembly(TWO_PARAM, SET_OP, "" + expr.getType().getSize(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o1");
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o0", "%o0");
        writeAssembly(LD_OP, "%o0", "%o0");
        writeAssembly(TWO_PARAM, "cmp", "%o0", "%o1");
        writeAssembly(BGE, ".$$.loopEnd."+MyParser.whileCounterStack.peek());
        writeAssembly(NOP);
        writeComment("iterVar = currentElem");
        writeAssembly(TWO_PARAM, SET_OP, iterVar.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, iterVar.getBase(), "%o1", "%o1");
        if(iterVar.isAccessFromArray() || iterVar.isPointer || iterVar.getRefSTO() != null)
        {
            writeAssembly(ST_OP, "%o0", "%o1");
        }
        else
        {
            writeAssembly(LD_OP, "%o0", "%o0");
            writeAssembly(ST_OP, "%o0", "%o1");
        }
        writeAssembly("\n");
        writeComment("start loop body");
        increaseIndent();
    }

    public void writeForeachEnd()
    {
        decreaseIndent();
        writeComment("End of loop body");
        writeAssembly(BA, ".$$.loopCheck." + MyParser.whileCounterStack.peek());
        writeAssembly(NOP);
        decreaseIndent();
        writeAssembly(LOCALVARDECL, ".$$.loopEnd." + MyParser.whileCounterStack.pop());
        writeAssembly("\n");
    }

    /*
     * break and continue
     */
    public void writeBreak()
    {
        writeAssembly(BA, ".$$.loopEnd." + MyParser.whileCounterStack.peek());
        writeAssembly(NOP);
        writeAssembly("\n");
    }

    public void writeContinue() {
        writeAssembly(BA, ".$$.loopCheck." + MyParser.whileCounterStack.peek());
        writeAssembly(NOP);
        writeAssembly("\n");
    }

    public void writeUnaryOp(STO a, String lastOffset, String op)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
        writeAssembly(LD_OP, "%l7", "%o0");

        if(op.equals("-"))
        {
            writeAssembly(TWO_PARAM, NEG, "%o0", "%o0");
        }
        if(op.equals("+"))
        {
            writeAssembly(TWO_PARAM, MOV_OP, "%o0", "%o0");
        }

        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void writeUnaryOpFloat(STO a, String lastOffset, String op)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
        writeAssembly(LD_OP, "%l7", "%f0");

        if(op.equals("-"))
        {
            writeAssembly(TWO_PARAM, "fnegs", "%f0", "%f0");
        }
        if(op.equals("+"))
        {
            writeAssembly(TWO_PARAM, "fmovs", "%f0", "%f0");
        }

        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%f0", "%o1");
        writeAssembly("\n");
    }

    public void writeAssign(STO a, STO b)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");
        if(a.isAccessFromArray() || a.getDeference() || a.getRefSTO() != null || a.isRef())
        {
            writeAssembly(LD_OP, "%o1", "%o1");
        }
        Type bType = b.getType();
        Type aType = a.getType();
        if(!(bType instanceof NullPointerType)) {
            if (b instanceof ConstSTO) {
                if (bType instanceof FloatType) {
                    const_count++;
                    writeRodataSec();
                    decreaseIndent();
                    writeAssembly(LOCALVARDECL, ".$$.float." + const_count);
                    increaseIndent();
                    writeAssembly(SINGLE, "" + ((ConstSTO) b).getFloatValue());
                    decreaseIndent();
                    writeTextSection();
                    writeAssembly(TWO_PARAM, SET_OP, ".$$.float." + const_count, "%l7");
                    writeAssembly(LD_OP, "%l7", "%f0");
                    writeAssembly(ST_OP, "%f0", "%o1");
                } else // int or bool
                {

                    if (aType instanceof IntType || aType instanceof BoolType) {
                        writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) b).getIntValue(), "%o0");
                        writeAssembly(ST_OP, "%o0", "%o1");
                    } else // aType is Float
                    {
                        writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) b).getIntValue(), "%o0");
                        MyParser.offset -= 4;
                        writeAssembly(TWO_PARAM, SET_OP, "" + MyParser.offset, "%l7");
                        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                        writeAssembly(ST_OP, "%o0", "%l7");
                        writeAssembly(LD_OP, "%l7", "%f0");
                        writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
                        writeAssembly(ST_OP, "%f0", "%o1");
                    }
                }
            }
            else // b is not Const
            {
                if (aType instanceof FloatType) {
                    if (bType instanceof FloatType) {
                        writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                        writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                        if (b.isAccessFromArray() || b.isPointer || b.getRefSTO()!= null || b.isRef()) {
                            writeAssembly(LD_OP, "%l7", "%l7");
                        }
                        writeAssembly(LD_OP, "%l7", "%f0");
                        writeAssembly(ST_OP, "%f0", "%o1");
                    } else {
                        writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                        writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                        if (b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef()) {
                            writeAssembly(LD_OP, "%l7", "%l7");
                        }
                        writeAssembly(LD_OP, "%l7", "%o0");
                        MyParser.offset -= 4;
                        writeAssembly(TWO_PARAM, SET_OP, "" + MyParser.offset, "%l7");
                        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                        writeAssembly(ST_OP, "%o0", "%l7");
                        writeAssembly(LD_OP, "%l7", "%f0");
                        writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
                        writeAssembly(ST_OP, "%f0", "%o1");
                    }
                }
                else // other than float
                {
                    writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
                    writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
                    if (b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef()) {
                        writeAssembly(LD_OP, "%l7", "%l7");
                    }
                    writeAssembly(LD_OP, "%l7", "%o0");
                    writeAssembly(ST_OP, "%o0", "%o1");
                }
            }
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, "0", "%o0");
            writeAssembly(ST_OP, "%o0", "%o1");
        }
        writeAssembly("\n");
    }


    /*
     * array
     */
    public void writeArrayAccess(STO a, STO index, String lastOffset)
    {
        if(index instanceof ConstSTO) // has value
        {
            writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) index).getIntValue(), "%o0");
        }
        else // unknown
        {
            writeAssembly(TWO_PARAM, SET_OP, index.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, index.getBase(), "%l7", "%l7");
            if(index.isAccessFromArray() || index.isPointer || index.getRefSTO() != null || index.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o0");
        }

        writeAssembly(TWO_PARAM, SET_OP, ""+((ArrayType)a.getType()).getDimension(), "%o1");
        writeAssembly(CALL_OP, ".$$.arrCheck");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, SET_OP, ""+((ArrayType)a.getType()).getElementType().getSize(), "%o1");
        writeAssembly(CALL_OP, ".mul");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, MOV_OP, "%o0", "%o1");
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o0", "%o0");
        if(a.getRefSTO()!=null){
            writeAssembly(LD_OP, "%o0", "%o0");
        }
        writeAssembly(CALL_OP, ".$$.ptrCheck");
        writeAssembly(NOP);
        writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void writeArrayAccessPointer(STO a, STO index, String lastOffset)
    {
        if(index instanceof ConstSTO) // has value
        {
            writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) index).getIntValue(), "%o0");
        }
        else // unknown
        {
            writeAssembly(TWO_PARAM, SET_OP, index.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, index.getBase(), "%l7", "%l7");
            if(index.isAccessFromArray() || index.isPointer || index.getRefSTO() != null || index.isRef())
            {
                writeAssembly(LD_OP, "%l7", "%l7");
            }
            writeAssembly(LD_OP, "%l7", "%o0");
        }

        writeAssembly(TWO_PARAM, SET_OP, ""+((PointerType)a.getType()).getElementType().getSize(), "%o1");
        writeAssembly(CALL_OP, ".mul");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, MOV_OP, "%o0", "%o1");
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(CALL_OP, ".$$.ptrCheck");
        writeAssembly(NOP);
        writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, lastOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");

    }


    /*
     * Typecast
     */
    public void writeConstToPtr(STO const_des, STO result)
    {
        if(const_des.getType() instanceof FloatType)
        {
            writeAssembly(RODATA_SEC);
            writeAssembly(ALIGN);
            const_count++;
            decreaseIndent();
            writeAssembly(LOCALVARDECL, ".$$.float." + const_count);
            increaseIndent();
            writeAssembly(SINGLE, "" + ((ConstSTO) const_des).getFloatValue());
            writeTextSection();
            writeAssembly(TWO_PARAM, SET_OP, ".$$.float." + const_count, "%l7");
            writeAssembly(LD_OP, "%l7", "%f0");
            writeAssembly(TWO_PARAM, "fstoi", "%f0", "%f0");
            writeAssembly(TWO_PARAM, SET_OP, result.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, result.getBase(), "%o1", "%o1");
            writeAssembly(ST_OP, "%f0", "%o1");
            writeAssembly("\n");
        }
        else // int or bool
        {
            writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO)const_des).getIntValue(), "%o0");
            writeAssembly(TWO_PARAM, SET_OP, result.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, result.getBase(), "%o1", "%o1");
            writeAssembly(ST_OP, "%o0", "%o1");
            writeAssembly("\n");
        }
    }

    public void writeVarToPtr(STO var_des, STO result)
    {
        writeAssembly(TWO_PARAM, SET_OP, var_des.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, var_des.getBase(), "%l7", "%l7");
        if(var_des.isAccessFromArray() || var_des.isPointer || var_des.getRefSTO() != null || var_des.isRef())
        {
            writeAssembly(LD_OP, "%l7", "%l7");
        }

        if(var_des.getType() instanceof FloatType)
        {
            writeAssembly(LD_OP, "%l7", "%f0");
            if(result.getType() instanceof IntType || result.getType() instanceof PointerType)
            {
                writeAssembly(TWO_PARAM, "fstoi", "%f0", "%f0");
            }

            if(result.getType() instanceof BoolType)
            {
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%g0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f1");
                writeAssembly(TWO_PARAM, "fitos", "%f1", "%f1");
                writeAssembly(TWO_PARAM, "fcmps", "%f0", "%f1");
                writeAssembly(NOP);
                MyParser.cmp_counter++;
                writeAssembly(FBE, ".$$.cmp." + MyParser.cmp_counter);
                writeAssembly(TWO_PARAM, "mov", "%g0", "%o0");
                writeAssembly(TWO_PARAM, "mov", "1", "%o0");
                decreaseIndent();
                writeAssembly(LOCALVARDECL, ".$$.cmp."+MyParser.cmp_counter);
                increaseIndent();
            }
            writeAssembly(TWO_PARAM, SET_OP, result.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, result.getBase(), "%o1", "%o1");
            if(result.getType() instanceof BoolType)
            {
                writeAssembly(ST_OP, "%o0", "%o1");
            }
            else
            {
                writeAssembly(ST_OP, "%f0", "%o1");
            }
            writeAssembly("\n");
        }
        else if(var_des.getType() instanceof BoolType)
        {
            writeAssembly(LD_OP, "%l7", "%o0");
            writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
            MyParser.cmp_counter++;
            writeAssembly(BE, ".$$.cmp."+MyParser.cmp_counter);
            writeAssembly(TWO_PARAM, "mov", "%g0", "%o0");
            writeAssembly(TWO_PARAM, "mov", "1", "%o0");
            decreaseIndent();
            writeAssembly(LOCALVARDECL, ".$$.cmp." + MyParser.cmp_counter);
            increaseIndent();

            if(result.getType() instanceof FloatType)
            {
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(TWO_PARAM, "fitos", "%f0", "%f0");
            }

            writeAssembly(TWO_PARAM, SET_OP, result.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, result.getBase(), "%o1", "%o1");
            if(result.getType() instanceof FloatType)
            {
                writeAssembly(ST_OP, "%f0", "%o1");
            }
            else
            {
                writeAssembly(ST_OP, "%o0", "%o1");
            }
        }
        else // int or ptr variables
        {
            writeAssembly(LD_OP, "%l7", "%o0");
            if(result.getType() instanceof BoolType)
            {
                writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
                MyParser.cmp_counter++;
                writeAssembly(BE, ".$$.cmp." + MyParser.cmp_counter);
                writeAssembly(TWO_PARAM, "mov", "%g0", "%o0");
                writeAssembly(TWO_PARAM, "mov", "1", "%o0");
                decreaseIndent();
                writeAssembly(LOCALVARDECL, ".$$.cmp." + MyParser.cmp_counter);
                increaseIndent();
            }
            if(result.getType() instanceof FloatType)
            {
                MyParser.offset -= 4;
                writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
                writeAssembly(ST_OP, "%o0", "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(TWO_PARAM, "fitos", "%f0", "%f0");
            }
            writeAssembly(TWO_PARAM, SET_OP, result.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, result.getBase(), "%o1", "%o1");
            if(result.getType() instanceof FloatType)
            {
                writeAssembly(ST_OP, "%f0", "%o1");
            }
            else
            {
                writeAssembly(ST_OP, "%o0", "%o1");
            }
        }
    }


    /*
     *   new Global stuff
     */
    public void writeGlobalDefine(boolean isStatic, String id, Type t, boolean isArray, String sizeofArray)
    {
        increaseIndent();
        writeAssembly("\n");
        writeAssembly(BSS_SEC);
        writeAssembly(ALIGN);
        if(!isStatic)
        {
            writeAssembly(GLOBALVARDECL, id);
        }
        decreaseIndent();
        writeAssembly(GLOBALVAR, id);
        increaseIndent();
        if(isArray)
        {
            writeAssembly(SKIP, sizeofArray);
        }
        else
        {
            writeAssembly(SKIP, ""+t.getSize());
        }
        writeTextSection();
    }

    public void writeGlobalInit(String id)
    {
        decreaseIndent();
        writeAssembly(GLOBALVAR, ".$.init." + id);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, "SAVE..$.init." + id, "%g1");
        writeAssembly(THREE_PARAM, "save", "%sp", "%g1", "%sp");
        writeAssembly("\n");
        increaseIndent();
    }

    public void writeInternalInit(String id)
    {
        writeAssembly(BSS_SEC);
        writeAssembly(ALIGN);
        decreaseIndent();
        writeAssembly(GLOBALVAR, ".$.init."+id);
        increaseIndent();
        writeAssembly(SKIP, "4");
        writeTextSection();
        writeComment("Start init guard");
        writeAssembly(TWO_PARAM, SET_OP, ".$.init." + id, "%o0");
        writeAssembly(LD_OP, "%o0", "%o0");
        writeAssembly(TWO_PARAM, "cmp\t", "%o0", "%g0");
        writeAssembly(BNE, ".$.init."+id+".done");
        writeAssembly(NOP);
        writeAssembly("\n");
    }

    public void writeGlobalInitEnd(String id)
    {
        writeComment("End of function .$.init." + id);
        writeAssembly(CALL_OP, ".$.init." + id + ".fini");
        writeAssembly(NOP);
        writeAssembly(RET);
        writeAssembly(RESTORE);
        writeAssembly(SAVESPACE, ".$.init." + id, "" + (-MyParser.offset));
        writeAssembly("\n");
        decreaseIndent();
        writeFini(id);
    }

    public void writeInternalInitEnd(String id)
    {
        writeComment("End init guard");
        writeAssembly(TWO_PARAM, SET_OP, ".$.init." + id, "%o0");
        writeAssembly(TWO_PARAM, MOV_OP, "1", "%o1");
        writeAssembly(ST_OP, "%o1", "%o0");
        decreaseIndent();
        writeAssembly(GLOBALVAR, ".$.init." + id + ".done");
        increaseIndent();
        writeAssembly("\n");
    }

    public void writeFini(String id)
    {
        decreaseIndent();
        writeAssembly(GLOBALVAR, ".$.init."+id+".fini");
        increaseIndent();
        writeAssembly(THREE_PARAM, "save", "%sp", "-96", "%sp");
        writeAssembly(RET);
        writeAssembly(RESTORE);
        writeAssembly("\n");
        writeAssembly(INIT_SEC);
        writeAssembly(ALIGN);
        writeAssembly(CALL_OP, ".$.init." + id);
        writeAssembly(NOP);
        writeTextSection();
        decreaseIndent();
    }

    /*
     * initialization Global
     */
    public void uninitGlobalBasic(String varName, String skipValue, boolean isStatic)
    {
        increaseIndent();
        writeAssembly("\n"); // new line just to make code clean
        writeAssembly(BSS_SEC);
        writeAssembly(ALIGN);
        if(!isStatic)
            writeAssembly(GLOBALVARDECL, varName);
        decreaseIndent();
        writeAssembly(GLOBALVAR, varName);
        increaseIndent();
        writeAssembly(SKIP, skipValue);
        decreaseIndent();

        writeTextSection(); // emulate reference compiler but idk why we need this
    }

    public void initGlobalBasic(String varName, String value ,boolean isFloat, boolean isStatic)
    {
        increaseIndent();
        writeAssembly("\n"); // new line just to make code clean
        writeAssembly(DATA_SEC);
        writeAssembly(ALIGN);
        if(!isStatic)
            writeAssembly(GLOBALVARDECL, varName);
        decreaseIndent();
        writeAssembly(GLOBALVAR, varName);
        increaseIndent();
        if(isFloat)
        {
            writeAssembly(SINGLE, value);
        }
        else // not float, so it is either bool or int
        {
            writeAssembly(WORD, value);
        }
        decreaseIndent();

        writeTextSection(); // emulate reference compiler but idk why we need this
    }

    public void initGlobalUnknowVar(String varName, String initName)
    {
        writeAssembly(GLOBALINITFUNC, varName);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, "SAVE..$.init." + varName, "%g1");
        writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "%g1", "%sp");
        increaseIndent();
        writeAssembly("\n");
        writeAssembly(COMMENT, varName + " = " + initName);
        writeAssembly(TWO_PARAM, SET_OP, varName, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%g0", "%o1", "%o1");
        writeAssembly(TWO_PARAM, SET_OP, initName, "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, "%g0", "%l7", "%l7");
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
        decreaseIndent();
        writeAssembly(COMMENT, ENDFUNC + ".$.init." + varName);
        writeAssembly(CALL_OP, ".$.init."+varName+".fini");
        writeAssembly(NOP);
        writeAssembly(RET);
        writeAssembly(RESTORE);
        writeAssembly(SAVESPACE, ".$.init."+varName, "0");
        writeAssembly("\n");
        decreaseIndent();
        writeAssembly(GLOBALINITFUNCFINI, varName);
        increaseIndent();
        writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
        writeAssembly(RET);
        writeAssembly(RESTORE);
        decreaseIndent();
        String initFuncName = ".$.init."+varName;
        writeInitSection(initFuncName);
        writeTextSection();
    }

    public void initGlobalUnknowVarFloat(String varName, String initName, boolean isInitFloat)
    {
        writeAssembly(GLOBALINITFUNC, varName);
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, "SAVE..$.init." + varName, "%g1");
        writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "%g1", "%sp");
        increaseIndent();
        writeAssembly("\n");
        writeAssembly(COMMENT, varName + " = " + initName);
        writeAssembly(TWO_PARAM, SET_OP, varName, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%g0", "%o1", "%o1");
        writeAssembly(TWO_PARAM, SET_OP, initName, "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, "%g0", "%l7", "%l7");
        if(isInitFloat) {
            writeAssembly(LD_OP, "%l7", "%f0");
            writeAssembly(ST_OP, "%f0", "%o1");
        }
        else
        {
            writeAssembly(LD_OP, "%l7", "%o0");
            writeAssembly(TWO_PARAM, SET_OP, "-4", "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
            writeAssembly(ST_OP, "%o0", "%l7");
            writeAssembly(LD_OP, "%l7", "%f0");
            writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
            writeAssembly(ST_OP, "%f0", "%o1");
        }
        writeAssembly("\n");
        decreaseIndent();
        writeAssembly(COMMENT, ENDFUNC+".$.init."+varName);
        writeAssembly(CALL_OP, ".$.init."+varName+".fini");
        writeAssembly(NOP);
        writeAssembly(RET);
        writeAssembly(RESTORE);
        if(isInitFloat)
            writeAssembly(SAVESPACE, ".$.init."+varName, "0");
        else
            writeAssembly(SAVESPACE, ".$.init."+varName, "-4");
        writeAssembly("\n");
        decreaseIndent();
        writeAssembly(GLOBALINITFUNCFINI, varName);
        increaseIndent();
        writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
        writeAssembly(RET);
        writeAssembly(RESTORE);
        decreaseIndent();
        String initFuncName = ".$.init."+varName;
        writeInitSection(initFuncName);
        writeTextSection();
    }




    public void initConst(STO a, String value)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");
        writeAssembly(TWO_PARAM, SET_OP, value, "%o0");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void initUnknownVar(STO a, STO b,  boolean isFloat)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");
        if(!(b.getType() instanceof NullPointerType))
        {
            writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
            if(!isFloat) {
                if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o0");
                writeAssembly(ST_OP, "%o0", "%o1");
            }
            else
            {
                if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(ST_OP, "%f0", "%o1");
            }
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, "0", "%o0");
            writeAssembly(ST_OP, "%o0", "%o1");
        }
        writeAssembly("\n");
    }

    public void initFloat(STO a, String value)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");
        const_count++;
        writeRodataSec();
        decreaseIndent();
        String localFloat = ".$$.float."+ const_count;
        writeAssembly(LOCALVARDECL, localFloat);
        increaseIndent();
        writeAssembly(SINGLE, value);
        decreaseIndent();
        writeTextSection();
        increaseIndent();
        writeAssembly(TWO_PARAM, SET_OP, localFloat, "%l7");
        writeAssembly(LD_OP, "%l7", "%f0");
        writeAssembly(ST_OP, "%f0", "%o1");
        writeAssembly("\n");
    }

    public void initInt2Float(STO a, STO b, String additionalOffset)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");
        writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
        if(b.isAccessFromArray() || b.isPointer || b.getRefSTO() != null || b.isRef())
        {
            writeAssembly(LD_OP, "%l7", "%l7");
        }
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, additionalOffset, "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
        writeAssembly(ST_OP, "%o0", "%l7");
        writeAssembly(LD_OP, "%l7", "%f0");
        writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
        writeAssembly(ST_OP, "%f0", "%o1");
        writeAssembly("\n");
    }


    public void initLocalExprFloat(STO a, STO b)
    {
        Type bType = b.getType();
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o1", "%o1");

        if(bType instanceof FloatType)
        {
            writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
            writeAssembly(LD_OP, "%l7", "%f0");
            writeAssembly(ST_OP, "%f0", "%o1");
        }
        else
        {
            writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%l7", "%l7");
            writeAssembly(LD_OP, "%l7", "%f0");
            MyParser.offset -= 4;
            writeAssembly(TWO_PARAM, SET_OP, ""+MyParser.offset, "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%l7", "%l7");
            writeAssembly(ST_OP, "%o0", "%l7");
            writeAssembly(LD_OP, "%l7", "%f0");
            writeAssembly(TWO_PARAM, FITOS, "%f0", "%f0");
            writeAssembly(ST_OP, "%f0", "%o1");
        }
        writeAssembly("\n");
    }

    public void writeEndl(){
        writeAssembly(TWO_PARAM, SET_OP, ".$$.strEndl", "%o0");
        writeAssembly(CALL_OP, "printf");
        writeAssembly(NOP);
    }

    public void writePrint(STO s){

        if(s instanceof ConstSTO){
            const_count++;
            if(s.getType() == null){ //A String
                writeAssembly(RODATA_SEC);
                writeAssembly(ALIGN);
                decreaseIndent();
                String temp = ".$$.str." + const_count;
                writeAssembly(temp + ":\n");
                increaseIndent();
                writeAssembly(".asciz" + SEPARATOR + "\"" + s.getName() + "\"");
                writeAssembly("\n");
                writeAssembly(TEXT_SEC);
                writeAssembly(ALIGN);
                writeComment("cout << \"" + s.getName() + "\"");
                writeAssembly(TWO_PARAM, SET_OP, ".$$.strFmt", "%o0");
                writeAssembly(TWO_PARAM, SET_OP, temp, "%o1");
                writeAssembly(CALL_OP, "printf");
                writeAssembly(NOP);
                writeAssembly("\n");
            }
            else if(s.getType() instanceof FloatType){
                writeAssembly(RODATA_SEC);
                writeAssembly(ALIGN);
                decreaseIndent();
                const_count++;
                String temp = ".$$.float." + const_count;
                writeAssembly(temp + ":\n");
                increaseIndent();
                writeAssembly(".single" + SEPARATOR + "0r" + ((ConstSTO) s).getFloatValue());
                writeAssembly("\n");
                writeAssembly(TEXT_SEC);
                writeAssembly(ALIGN);
                writeAssembly(TWO_PARAM, SET_OP, temp, "%l7");
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(CALL_OP, "printFloat");
                writeAssembly(NOP);
                writeAssembly("\n");
            }
            else if(s.getType() instanceof IntType){
                int value = ((ConstSTO) s).getIntValue();
                writeAssembly(TWO_PARAM, SET_OP, ""+value, "%o1");
                writeAssembly(TWO_PARAM, SET_OP, ".$$.intFmt", "%o0");
                writeAssembly(CALL_OP, "printf");
                writeAssembly(NOP);
                writeAssembly("\n");
            }
            else if(s.getType() instanceof BoolType)
            {
                int value = ((ConstSTO) s).getIntValue();
                writeAssembly(TWO_PARAM, SET_OP, ""+value, "%o0");
                writeAssembly(CALL_OP, ".$$.printBool");
                writeAssembly(NOP);
                writeAssembly("\n");
            }

        }
        else{
            String offset = s.getOffset();
            writeAssembly(TWO_PARAM, SET_OP, offset, "%l7");
            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%l7", "%l7");
            STO ss = s.getRefSTO();
            if(s.getType().isInt()) {
                if(s.isAccessFromArray() || s.isRef() || ss!=null )
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o1");
                writeAssembly(TWO_PARAM, SET_OP, ".$$.intFmt", "%o0");
                writeAssembly(CALL_OP, "printf");
                writeAssembly(NOP);
            }
            else if(s.getType() instanceof BoolType){
                if(s.isAccessFromArray() || s.isRef() || ss!=null )
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%o0");
                writeAssembly(CALL_OP, ".$$.printBool");
                writeAssembly(NOP);
            }
            else if(s.getType() instanceof FloatType){
                if(s.isAccessFromArray() || s.isRef() || ss!=null )
                {
                    writeAssembly(LD_OP, "%l7", "%l7");
                }
                writeAssembly(LD_OP, "%l7", "%f0");
                writeAssembly(CALL_OP, "printFloat");
                writeAssembly(NOP);
            }

        }
    }

    public void writeCin(STO s){
        writeComment("cin >> " + s.getName());
        if(s.getType() instanceof IntType){
            writeAssembly(CALL_OP, "inputInt");
            writeAssembly(NOP);
            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o1", "%o1");
            if(s.isAccessFromArray() || s.getRefSTO() != null || s.isPointer){
                writeAssembly(LD_OP, "%o1", "%o1");
            }
            writeAssembly(ST_OP, "%o0", "%o1");
        }
        else if(s.getType() instanceof FloatType){
            writeAssembly(CALL_OP, "inputFloat");
            writeAssembly(NOP);
            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o1", "%o1");
            if(s.isAccessFromArray()){
                writeAssembly(LD_OP, "%o1", "%o1");
            }
            writeAssembly(ST_OP, "%f0", "%o1");
        }
    }


    /*
     * pointer stuff
     */
    public void writeAddressOf(STO a, STO b)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%o0", "%o0");
        if(a.isAccessFromArray() || a.getRefSTO()!=null)
        {
            writeAssembly(LD_OP, "%o0", "%o0");
        }
        writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void writePtrDere(STO a, STO b)
    {
        writeAssembly(TWO_PARAM, SET_OP, a.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, a.getBase(), "%l7", "%l7");
        if(a.isAccessFromArray())
        {
            writeAssembly(LD_OP, "%l7", "%l7");
        }
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(CALL_OP, ".$$.ptrCheck");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, SET_OP, b.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, b.getBase(), "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }



    public void writeParam(String paramName, int offset){
        writeAssembly(ST_OP, paramName, "%fp+" + offset);
    }

    public void writeFunCall(FuncSTO sto, boolean def){
        //run through the parameter, if constSTO, store to o1...
        Vector<STO> p = sto.getParamList();
        String name = sto.getName();
        String id = name;
        if(def){
            STO s = sto.getRefSTO();
            if(s.isPointer){
                writePointer(s, Integer.valueOf(sto.getOffset()));
            }
            name = sto.accessStructType().getName() + "." + name;
            writeComment(sto.getFullName() + "(...)");
        }
        else {
            writeComment(name + "(...)");
        }
        if(def){
            STO s = sto.getRefSTO();
            if(s.isPointer){
                writeAssembly(TWO_PARAM, SET_OP, sto.getOffset(), "%o0");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o0", "%o0");
                writeAssembly(LD_OP, "%o0", "%o0");
            }
            else if(s != null){
                writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o0");
                writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o0", "%o0");
            }
        }
            if (p == null || p.size() == 0) {
                name = name + ".void";

            } else {
                writeComment("Setting Parameter");
                for (int k = 0; k < p.size(); k++) {
                    STO s = p.get(k);
                    int i = k;
                    if (def) {
                        i = k + 1;
                    }
                    if (s.getType() instanceof PointerType) {
                        PointerType pp = (PointerType) s.getType();
                        name = name + "." + pp.getElementType().getName() + "$";
                    } else if (s.getType() instanceof ArrayType) {
                        ArrayType aa = (ArrayType) s.getType();
                        int dim = aa.getDimension();
                        String temp = "$" + dim + "$";
                        while (aa.getElementType() instanceof ArrayType) {
                            ArrayType atmp = (ArrayType) aa.getElementType();
                            dim = atmp.getDimension();
                            temp = temp + "$" + dim + "$";
                            aa = atmp;
                        }
                        temp = aa.getElementType().getName() + temp;
                        name = name + "." + temp;
                    } else {
                        name = name + "." + s.getType().getName();
                    }
                    if (s instanceof ConstSTO) {
                        if (s.getType() instanceof FloatType) {
                            const_count++;
                            writeAssembly(RODATA_SEC);
                            writeAssembly(ALIGN);
                            decreaseIndent();
                            String temp = ".$$.float." + const_count;
                            writeAssembly(temp + ":\n");
                            increaseIndent();
                            writeAssembly(".single" + SEPARATOR + "0r" + ((ConstSTO) s).getFloatValue());
                            writeAssembly("\n");
                            writeAssembly(TEXT_SEC);
                            writeAssembly(ALIGN);
                            writeAssembly(TWO_PARAM, SET_OP, temp, "%l7");
                            writeAssembly(LD_OP, "%l7", "%f" + i);
                        } else if (s.getType() instanceof BoolType || s.getType() instanceof IntType) {
                            writeAssembly(TWO_PARAM, SET_OP, "" + ((ConstSTO) s).getIntValue(), "%o" + i);
                        } else {
                            writeAssembly(TWO_PARAM, SET_OP, s.getName(), "%o" + i);
                        }
                    } else {
                        if (sto.getRef() || s.isRef() || s.getType() instanceof StructType) {
                            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o" + i);
                            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o" + i, "%o" + i);
                        } else if (s.getType() instanceof ArrayType) {
                            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o" + i);
                            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o" + i, "%o" + i);
                            writeAssembly(LD_OP, "%o" + i, "%o" + i);
                        } else {
                            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%l7");
                            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%l7", "%l7");
                            if(s.isAccessFromArray()){
                                writeAssembly(LD_OP, "%l7", "%l7");
                            }
                            if (s.getType() instanceof FloatType) {
                                writeAssembly(LD_OP, "%l7", "%f" + i);
                            } else {
                                writeAssembly(LD_OP, "%l7", "%o" + i);
                            }
                        }
                    }
                }

        }

        if(sto.isExtern){
            name = id;
        }
        writeAssembly(CALL_OP, name);
        writeAssembly(NOP);
        if(def){
            writeAssembly("\n");
        }
        else {
            writeAssembly(TWO_PARAM, SET_OP, sto.getOffset(), "%o1");
            writeAssembly(THREE_PARAM, ADD_OP, sto.getBase(), "%o1", "%o1");
            if (sto.getType() instanceof FloatType && !sto.getRef()) {
                writeAssembly(ST_OP, "%f0", "%o1");
            } else {
                writeAssembly(ST_OP, "%o0", "%o1");
            }
            writeAssembly("\n");
        }
    }


    public void writeStaticStructParam(String name, Type st, Vector<STO> p, Vector<STO> array, boolean g, boolean init){
        increaseIndent();
        writeAssembly(BSS_SEC);
        writeAssembly(ALIGN);
        if(g){
            writeAssembly(".global\t" + name + "\n");
        }
        decreaseIndent();
        writeAssembly(name + ":\n");
        increaseIndent();
        int offset = 0;
        if(array==null) {
            writeAssembly(SKIP, "" + st.getSize());
        }
        else{
            int sizeOfArray = st.getSize();
            offset = 4;
            for(int i = 0; i < array.size(); i++)
            {
                STO curIndex = array.get(i);
                sizeOfArray *= ((ConstSTO)curIndex).getIntValue();
                offset *= ((ConstSTO)curIndex).getIntValue();
            }
            writeAssembly(SKIP, "" + sizeOfArray);
        }
        writeAssembly("\n");
        writeAssembly(TEXT_SEC);
        writeAssembly(ALIGN);
        writeAssembly("\n");
        String tmp;
        if(init){
            tmp = ".$.init." + name;
            writeAssembly(BSS_SEC);
            writeAssembly(ALIGN);
            decreaseIndent();
            writeAssembly(tmp + ":\n");
            increaseIndent();
            writeAssembly(SKIP, "4");
            writeAssembly(TEXT_SEC);
            writeAssembly(ALIGN);
            writeComment("Start init guard");

            writeAssembly(TWO_PARAM, SET_OP, tmp, "%o0");
            writeAssembly(LD_OP, "%o0", "%o0");
            writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
            writeAssembly(BNE, tmp+".done");
            writeAssembly(NOP);
        }
        else {
            decreaseIndent();
            tmp = ".$.init." + name;
            writeAssembly(tmp + ":\n");
            increaseIndent();
            writeAssembly(TWO_PARAM, SET_OP, "SAVE." + tmp, "%g1");
            writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "%g1", "%sp");
            writeAssembly("\n");
        }
        increaseIndent();
        writeStructParam(name, st, p, array, name, "%g0", true);
        decreaseIndent();
        if(init){
            writeComment("End init guard");
            writeAssembly(TWO_PARAM, SET_OP, tmp, "%o0");
            writeAssembly(TWO_PARAM, MOV_OP, "1", "%o1");
            writeAssembly(ST_OP, "%o1", "%o0");
            decreaseIndent();
            writeAssembly(tmp + ".done:\n");
            increaseIndent();
        }
        else {
        writeComment("End of function " + tmp);
        writeAssembly(CALL_OP, tmp+".fini");
        writeAssembly(NOP);
        writeAssembly(RET);
        writeAssembly(RESTORE);
        writeAssembly("SAVE." + tmp + " = -(92 + " + offset + ") & -8");
        writeAssembly("\n");
        decreaseIndent();


            writeAssembly(tmp + ".fini:\n");
            increaseIndent();
            writeAssembly(THREE_PARAM, SAVE_OP, "%sp", "-96", "%sp");
            writeAssembly(RET);
            writeAssembly(RESTORE);
            writeAssembly("\n");
            writeAssembly(INIT_SEC);
            writeAssembly(ALIGN);
            writeAssembly(CALL_OP, tmp);
            writeAssembly(NOP);
            writeAssembly("\n");
            writeAssembly(TEXT_SEC);
            writeAssembly(ALIGN);
            decreaseIndent();
        }
    }

    public void writeStructParam(String name, Type st, Vector<STO> p, Vector<STO> array, String offset, String base, boolean varS) {
        if (array == null) {
            writeComment(name + "." + st.getName() + "(...)");
            writeAssembly(TWO_PARAM, SET_OP, "" + offset, "%o0");
            writeAssembly(THREE_PARAM, ADD_OP, base, "%o0", "%o0");
            String fullname = st.getName() + "." + st.getName();
            if (p == null || p.size() == 0) {
                fullname = fullname + ".void";
            } else {
                for (int i = 0; i < p.size(); i++) {
                    STO s = p.get(i);
                    if (s.getType() instanceof PointerType) {
                        PointerType pp = (PointerType) s.getType();
                        fullname = fullname + "." + pp.getElementType().getName() + "$";
                    } else if (s.getType() instanceof ArrayType) {
                        ArrayType aa = (ArrayType) s.getType();
                        int dim = aa.getDimension();
                        String temp = "$" + dim + "$";
                        while (aa.getElementType() instanceof ArrayType) {
                            ArrayType atmp = (ArrayType) aa.getElementType();
                            dim = atmp.getDimension();
                            temp = temp + "$" + dim + "$";
                            aa = atmp;
                        }
                        temp = aa.getElementType().getName() + temp;
                        fullname = fullname + "." + temp;
                    } else {
                        fullname = fullname + "." + s.getType().getName();
                    }
                    int k = i + 1;
                    if (s instanceof ConstSTO) {
                        if (s.getType() instanceof FloatType) {
                            const_count++;
                            writeAssembly(RODATA_SEC);
                            writeAssembly(ALIGN);
                            decreaseIndent();
                            String temp = ".$$.float." + const_count;
                            writeAssembly(temp + ":\n");
                            increaseIndent();
                            writeAssembly(".single" + SEPARATOR + "0r" + ((ConstSTO) s).getFloatValue());
                            writeAssembly("\n");
                            writeAssembly(TEXT_SEC);
                            writeAssembly(ALIGN);
                            writeAssembly(TWO_PARAM, SET_OP, temp, "%l7");
                            writeAssembly(LD_OP, "%l7", "%f" + k);
                        }
                        else if(s.getType() instanceof BoolType || s.getType() instanceof IntType){
                            writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) s).getIntValue(), "%o"+k);
                        }else {
                            String tmp = s.getName();
                            writeAssembly(TWO_PARAM, SET_OP, tmp, "%o" + k);
                        }
                    } else {
                        if (s.isRef() || s.getType() instanceof StructType) {
                            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o" + k);
                            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o" + k, "%o" + k);
                        } else if (s.getType() instanceof ArrayType) {
                            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o" + k);
                            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o" + k, "%o" + k);
                            writeAssembly(LD_OP, "%o" + k, "%o" + k);
                        } else {
                            writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%l7");
                            writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%l7", "%l7");
                            if(s.isAccessFromArray()){
                                writeAssembly(LD_OP, "%l7", "%l7");
                            }
                            if (s.getType() instanceof FloatType) {
                                writeAssembly(LD_OP, "%l7", "%f" + k);
                            } else {
                                writeAssembly(LD_OP, "%l7", "%o" + k);
                            }
                        }
                    }
                }
            }
                writeAssembly(CALL_OP, fullname);
                writeAssembly(NOP);
                writeAssembly("\n");
                writeAssembly(BSS_SEC);
                writeAssembly(ALIGN);
                decreaseIndent();
                STO s = new FuncSTO(st.getName(), st, 0);
                s.isStatic = varS;
            st_count++;
                String ctor = ".$$.ctorDtor." + st_count;
            s.setFullName(ctor);
            if(varS){
                global_static_struct.add(s);
            }
            else {
                struct_count.add(s);
            }
                writeAssembly(ctor + ":\n");
                increaseIndent();
                writeAssembly(SKIP, "4");
                writeAssembly("\n");
                writeAssembly(TEXT_SEC);
                writeAssembly(ALIGN);
                writeAssembly("\n");
                writeAssembly(TWO_PARAM, SET_OP, ctor, "%o0");
                writeAssembly(TWO_PARAM, SET_OP, "" + offset, "%o1");
                writeAssembly(THREE_PARAM, ADD_OP, base, "%o1", "%o1");
                writeAssembly(ST_OP, "%o1", "%o0");
                writeAssembly("\n");
            }

            else{
                int sizeOfArray = 1;
                for (int i = 0; i < array.size(); i++) {
                    STO curIndex = array.get(i);
                    sizeOfArray *= ((ConstSTO) curIndex).getIntValue();
                }
                int off;
                if (varS) {
                    off = 0;
                } else {
                    off = Integer.valueOf(offset);
                }
                for (int k = 0; k < sizeOfArray; k++) {
                    String fullname = name + "[" + k + "]";
                    writeComment(fullname);
                    writeAssembly(TWO_PARAM, SET_OP, "" + k, "%o0");
                    writeAssembly(TWO_PARAM, SET_OP, "" + sizeOfArray, "%o1");
                    writeAssembly(CALL_OP, ".$$.arrCheck");
                    writeAssembly(NOP);
                    writeAssembly(TWO_PARAM, SET_OP, "" + st.getSize(), "%o1");
                    writeAssembly(CALL_OP, ".mul");
                    writeAssembly(NOP);
                    writeAssembly(TWO_PARAM, MOV_OP, "%o0", "%o1");
                    if (varS) {
                        writeAssembly(TWO_PARAM, SET_OP, name, "%o0");
                        writeAssembly(THREE_PARAM, ADD_OP, "%g0", "%o0", "%o0");
                    } else {
                        writeAssembly(TWO_PARAM, SET_OP, offset, "%o0");
                        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o0", "%o0");
                    }
                    writeAssembly(CALL_OP, ".$$.ptrCheck");
                    writeAssembly(NOP);
                    writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o0");
                    off -= 4;
                    if (!varS) {
                        MyParser.offset -= 4;
                    }
                    writeAssembly(TWO_PARAM, SET_OP, "" + off, "%o1");
                    writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
                    writeAssembly(ST_OP, "%o0", "%o1");
                    writeAssembly("\n");

                    // Array[k].STRUCT(...)
                    writeComment(fullname + "." + st.getName() + "(...)");
                    writeAssembly(TWO_PARAM, SET_OP, "" + off, "%o0");
                    writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o0", "%o0");
                    writeAssembly(LD_OP, "%o0", "%o0");
                    fullname = st.getName() + "." + st.getName();
                    if (p == null || p.size() == 0) {
                        fullname = fullname + ".void";
                    } else {
                        for (int i = 0; i < p.size(); i++) {
                            STO s = p.get(i);
                            if (s.getType() instanceof PointerType) {
                                PointerType pp = (PointerType) s.getType();
                                fullname = fullname + "." + pp.getElementType().getName() + "$";
                            } else if (s.getType() instanceof ArrayType) {
                                ArrayType aa = (ArrayType) s.getType();
                                int dim = aa.getDimension();
                                String temp = "$" + dim + "$";
                                while (aa.getElementType() instanceof ArrayType) {
                                    ArrayType atmp = (ArrayType) aa.getElementType();
                                    dim = atmp.getDimension();
                                    temp = temp + "$" + dim + "$";
                                    aa = atmp;
                                }
                                temp = aa.getElementType().getName() + temp;
                                fullname = fullname + "." + temp;
                            } else {
                                fullname = fullname + "." + s.getType().getName();
                            }
                            int kk = i + 1;
                            if (s instanceof ConstSTO) {
                                if (s.getType() instanceof FloatType) {
                                    const_count++;
                                    writeAssembly(RODATA_SEC);
                                    writeAssembly(ALIGN);
                                    decreaseIndent();
                                    String temp = ".$$.float." + const_count;
                                    writeAssembly(temp + ":\n");
                                    increaseIndent();
                                    writeAssembly(".single" + SEPARATOR + "0r" + ((ConstSTO) s).getFloatValue());
                                    writeAssembly("\n");
                                    writeAssembly(TEXT_SEC);
                                    writeAssembly(ALIGN);
                                    writeAssembly(TWO_PARAM, SET_OP, temp, "%l7");
                                    writeAssembly(LD_OP, "%l7", "%f" + kk);
                                }
                                else if(s.getType() instanceof BoolType || s.getType() instanceof IntType){
                                    writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) s).getIntValue(), "%o"+kk);
                                }else {
                                    String tmp = s.getName();
                                    writeAssembly(TWO_PARAM, SET_OP, tmp, "%o"+kk);
                                }
                            }
                            else {
                                if (s.isRef() || s.getType() instanceof StructType) {
                                    writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o" + kk);
                                    writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o" + kk, "%o" + kk);
                                } else if (s.getType() instanceof ArrayType) {
                                    writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o" + kk);
                                    writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o" + kk, "%o" + kk);
                                    writeAssembly(LD_OP, "%o" + kk, "%o" + kk);
                                } else {
                                    writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%l7");
                                    writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%l7", "%l7");
                                    if (s.getType() instanceof FloatType) {
                                        writeAssembly(LD_OP, "%l7", "%f" + kk);
                                    } else {
                                        writeAssembly(LD_OP, "%l7", "%o" + kk);
                                    }
                                }
                        }
                    }
                }
                    writeAssembly(CALL_OP, fullname);
                    writeAssembly(NOP);
                    writeAssembly("\n");
                    writeAssembly(BSS_SEC);
                    writeAssembly(ALIGN);
                    decreaseIndent();
                    STO s = new FuncSTO(st.getName(), st, 0);
                    st_count++;
                    String ctor = ".$$.ctorDtor." + st_count;
                    s.setFullName(ctor);
                    if(varS){
                        global_static_struct.add(s);
                    }
                    else {
                        struct_count.add(s);
                    }
                    writeAssembly(ctor + ":\n");
                    increaseIndent();
                    writeAssembly(SKIP, "4");
                    writeAssembly("\n");
                    writeAssembly(TEXT_SEC);
                    writeAssembly(ALIGN);
                    writeAssembly("\n");
                    writeAssembly(TWO_PARAM, SET_OP, ctor, "%o0");
                    writeAssembly(TWO_PARAM, SET_OP, "" + off, "%o1");
                    writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
                    writeAssembly(LD_OP, "%o1", "%o1");
                    writeAssembly(ST_OP, "%o1", "%o0");
                    writeAssembly("\n");
            }
        }
    }

    public void writeDotAccess(STO sto, int offset){
        writeComment(sto.getFullName());
        STO s = sto.getRefSTO();
        writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o0", "%o0");
        if((s!=null && s.isThis()) || s.isAccessFromArray()){
            writeAssembly(LD_OP, "%o0", "%o0");
        }
        writeAssembly(TWO_PARAM, SET_OP, ""+sto.strucOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%g0", "%o1", "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, ""+offset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");

        writeAssembly("\n");
    }

    public void writeNewStmt(STO sto){
        writeComment("new( " + sto.getName() + " )");
        writeAssembly(TWO_PARAM, MOV_OP, "1", "%o0");
        int size;
        if(sto.isRef() || (sto.getType() instanceof PointerType)){
            PointerType pp = (PointerType) sto.getType();
            Type t = pp.getElementType();
            size = t.getSize();
        }
        else{
            size = sto.getType().getSize();
        }
        writeAssembly(TWO_PARAM, SET_OP, ""+size, "%o1");
        writeAssembly(CALL_OP, "calloc");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, SET_OP, sto.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, sto.getBase(), "%o1", "%o1");
        if(sto.isAccessFromArray())
        {
            writeAssembly(LD_OP, "%o1", "%o1");
        }
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }


    public void writePointer(STO sto, int offset){
        writeComment("*" + sto.getName());
        writeAssembly(TWO_PARAM, SET_OP, sto.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, sto.getBase(), "%l7", "%l7");
        if(sto.getRefSTO()!=null || sto.isRef()){
            writeAssembly(LD_OP, "%l7", "%l7");
        }
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(CALL_OP, ".$$.ptrCheck");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, SET_OP, "" + offset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }
    public void writeNewStmtStruct(STO sto, FuncSTO f, int offset){
        writePointer(sto, offset);
        Type t = f.getType();
        writeComment("*" + sto.getName() + "." + t.getName() + "(...)");
        writeAssembly(TWO_PARAM, SET_OP, "" + offset, "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, f.getBase(), "%o0", "%o0");
        writeAssembly(LD_OP, "%o0", "%o0");
        Vector<STO> p = f.getParamList();
        writeComment("Setting parameter");
        String name = t.getName() + "." + t.getName();
        if(p == null || p.size() == 0){
            name = name + ".void";
        }
        for (int k = 0; k < p.size(); k++){
            STO s = p.get(k);
            int i = k+1;
            if(s.getType() instanceof PointerType){
                PointerType pp = (PointerType) s.getType();
                name = name + "." + pp.getElementType().getName()+"$";
            }
            else {
                name = name + "." + s.getType().getName();
            }
            if(s instanceof ConstSTO){
                if(s.getType() instanceof FloatType){
                    const_count++;
                    writeAssembly(RODATA_SEC);
                    writeAssembly(ALIGN);
                    decreaseIndent();
                    String temp = ".$$.float." + const_count;
                    writeAssembly(temp + ":\n");
                    increaseIndent();
                    writeAssembly(".single" + SEPARATOR + "0r" + ((ConstSTO) s).getFloatValue());
                    writeAssembly("\n");
                    writeAssembly(TEXT_SEC);
                    writeAssembly(ALIGN);
                    writeAssembly(TWO_PARAM, SET_OP, temp, "%l7");
                    writeAssembly(LD_OP, "%l7", "%f"+i);
                }
                else if(s.getType() instanceof BoolType || s.getType() instanceof IntType){
                    writeAssembly(TWO_PARAM, SET_OP, ""+((ConstSTO) s).getIntValue(), "%o"+i);
                }
                else{
                    writeAssembly(TWO_PARAM, SET_OP, s.getName(), "%o"+i);
                }
            }
            else{
                if(f.getRef() || s.isRef()){
                    writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%o" + i);
                    writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%o"+ i, "%o" + i);
                }
                else {
                    writeAssembly(TWO_PARAM, SET_OP, s.getOffset(), "%l7");
                    writeAssembly(THREE_PARAM, ADD_OP, s.getBase(), "%l7", "%l7");
                    if(s.isAccessFromArray()){
                        writeAssembly(LD_OP, "%l7", "%l7");
                    }
                    if (s.getType() instanceof FloatType) {
                        writeAssembly(LD_OP, "%l7", "%f" + i);
                    } else {
                        writeAssembly(LD_OP, "%l7", "%o" + i);
                    }
                }
            }
        }
        writeAssembly(CALL_OP, name);
        writeAssembly(NOP);
        writeAssembly("\n");
    }

    public void writeEndNewStruct(STO sto, int offset){
        PointerType p = (PointerType)sto.getType();
        Type t = p.getElementType();
        writePointer(sto, offset);
        writeComment(sto.getName() + ".~" + t.getName() + "(...)");
        writeAssembly(TWO_PARAM, SET_OP, ""+offset, "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, sto.getBase(), "%o0", "%o0");
        writeAssembly(LD_OP, "%o0", "%o0");
        writeAssembly(CALL_OP, t.getName() + ".$" + t.getName() + ".void");
        writeAssembly(NOP);
        writeAssembly("\n");
    }

    public void writeArrowAccess(STO sto, int offset) {
        writePointer(sto.getRefSTO(), offset);
        writeComment("*" + sto.getRefSTO().getName() + "." + sto.getName());
        writeAssembly(TWO_PARAM, SET_OP, "" + offset, "%o0");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o0", "%o0");
        writeAssembly(LD_OP, "%o0", "%o0");
        writeAssembly(TWO_PARAM, SET_OP, ""+sto.strucOffset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%g0", "%o1", "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%o0", "%o1", "%o0");
        offset-=4;
        writeAssembly(TWO_PARAM, SET_OP, ""+offset, "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, "%fp", "%o1", "%o1");
        writeAssembly(ST_OP, "%o0", "%o1");
        writeAssembly("\n");
    }

    public void writeDeleteStmt(STO sto, int offset){
        PointerType p = (PointerType) sto.getType();
        if(p.getElementType() instanceof StructType){
            writeEndNewStruct(sto, offset);
        }
        writeComment("delete( " + sto.getName() +  " )");
        writeAssembly(TWO_PARAM, SET_OP, sto.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, sto.getBase(), "%l7", "%l7");
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(CALL_OP, ".$$.ptrCheck");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, SET_OP, sto.getOffset(), "%l7");
        writeAssembly(THREE_PARAM, ADD_OP, sto.getBase(), "%l7", "%l7");
        writeAssembly(LD_OP, "%l7", "%o0");
        writeAssembly(CALL_OP, "free");
        writeAssembly(NOP);
        writeAssembly(TWO_PARAM, SET_OP, sto.getOffset(), "%o1");
        writeAssembly(THREE_PARAM, ADD_OP, sto.getBase(), "%o1", "%o1");
        writeAssembly(ST_OP, "%g0", "%o1");
        writeAssembly("\n");
    }



    // reference of how to use the class
   /* public static void main(String args[]) {
        AssemblyCodeGenerator myAsWriter = new AssemblyCodeGenerator("rc.s");

        myAsWriter.increaseIndent();
        myAsWriter.writeAssembly(TWO_PARAM, SET_OP, String.valueOf(4095), "%l0");
        myAsWriter.increaseIndent();
        myAsWriter.writeAssembly(TWO_PARAM, SET_OP, String.valueOf(1024), "%l1");
        myAsWriter.decreaseIndent();

        myAsWriter.writeAssembly(TWO_PARAM, SET_OP, String.valueOf(512), "%l2");

        myAsWriter.decreaseIndent();
        myAsWriter.dispose();
    }*/
}
//---------------------------------------------------------------------
// CSE 131 Reduced-C Compiler Project
// Copyright (C) 2008-2015 Garo Bournoutian and Rick Ord
// University of California, San Diego
//---------------------------------------------------------------------

import java.util.Vector;

class FuncSTO extends STO
{
	private Type m_returnType;
	private Vector<STO> paramList = new Vector<STO>();
	private boolean m_declared = false;
	private boolean m_has_return = false;
	private boolean overloaded = false;
	private boolean optref = false;
	private boolean struct_member = false;
	private StructType f_struct = null;
	private String f_assemblyName = "";


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public FuncSTO(String strName, Type t, int symLevel)
	{
		super (strName);
		setReturnType(t);
		m_declared = true;
		//paramList = params;
		// You may want to change the isModifiable and isAddressable                      
		// fields as necessary
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean isFunc() 
	{ 
		return true;
		// You may want to change the isModifiable and isAddressable                      
		// fields as necessary

	}

	public void setParam(Vector<STO> s){
		paramList = s;
	}

	public Vector<STO> getParamList(){
		return paramList;
	}



	public boolean hasReturn() { return m_has_return; }
	public void setReturn(boolean b) {m_has_return = b;}


	//@Overide
	public Type getType() {return m_returnType; }


	public boolean isOverloaded(){return overloaded; }
	public void setOverload(boolean b ){ overloaded = b; }


	//----------------------------------------------------------------
	// This is the return type of the function. This is different from 
	// the function's type (for function pointers - which we are not 
	// testing in this project).
	//----------------------------------------------------------------
	public void setReturnType(Type typ)
	{
		m_returnType = typ;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Type getReturnType ()
	{
		return m_returnType;
	}

	public void setRef(boolean ref){
		optref = ref;
		setIsAddressable(ref);
		setIsModifiable(ref);
	}
	public boolean getRef(){
		return optref;
	}
	public boolean isRef(){
		return optref;
	}

	public void setStruct_member(boolean b){
		struct_member = b;
	}
	public boolean getStruct_member(){
		return struct_member;
	}

	public StructType accessStructType(){
		return f_struct;
	}
	public void setStructType(StructType s){
		f_struct = s;
	}
	public void setAssemblyName(String s) { f_assemblyName = s;}


}

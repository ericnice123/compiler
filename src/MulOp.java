/**
 * Created by Tsung-Huan on 4/8/15.
 */
class MulOp extends ArithmeticOp{
    public MulOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a, STO b)
    {
        Type aType = a.getType();
        Type bType = b.getType();

        if(!(aType instanceof NumericType) || !(bType instanceof NumericType))
        {
            if(!(aType instanceof NumericType))
                return new ErrorSTO(aType.getName());
            else    // if bType isn't NumericType
                return new ErrorSTO(bType.getName());
        }
        else if(aType instanceof IntType && bType instanceof IntType)
        {
            if(!(a instanceof ConstSTO) || !(b instanceof ConstSTO)) {
                ExprSTO exprSTO = new ExprSTO(a.getName() + " * " +b.getName(), new IntType());
                MyParser.offset -= 4;
                exprSTO.setOffset(""+MyParser.offset);
                exprSTO.setBase("%fp");
                MyParser.m_acg.writeComment(a.getName() + " * " + b.getName());
                MyParser.m_acg.LLCS(a, b, "" + MyParser.offset, "*");
                return exprSTO;
            }
            else
            {
                int result = ((ConstSTO) a).getIntValue() * ((ConstSTO) b).getIntValue();
                return new ConstSTO(""+result, new IntType(), result);
            }
        }
        else
        {
            if(!(a instanceof ConstSTO) || !(b instanceof ConstSTO)) {
                ExprSTO exprSTO = new ExprSTO(a.getName() + " * " +b.getName(), new FloatType());
                MyParser.offset -= 4;
                exprSTO.setOffset(""+MyParser.offset);
                exprSTO.setBase("%fp");
                MyParser.m_acg.writeComment(a.getName() + " * " + b.getName());
                MyParser.m_acg.LLCSFloat(a, b, "" + MyParser.offset, "*");
                return exprSTO;
            }
            else
            {
                float result = ((ConstSTO) a).getFloatValue() * ((ConstSTO) b).getFloatValue();
                return new ConstSTO(""+result, new FloatType(), result);
            }
        }
    }
}

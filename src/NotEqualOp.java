/**
 * Created by Dennis on 4/9/2015.
 */
class NotEqualOp extends ComparisonOp {
    public NotEqualOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a, STO b)
    {
        Type aType = a.getType();
        Type bType = b.getType();

        if((aType instanceof NumericType && bType instanceof NumericType))
        {
            if(a instanceof ConstSTO && b instanceof ConstSTO)
            {
                int result = ((ConstSTO) a).getValue().compareTo(((ConstSTO) b).getValue());
                if(result != 0) {
                    return new ConstSTO("true", new BoolType(), 1);
                }
                else
                {
                    return new ConstSTO("false", new BoolType(), 0);
                }
            }
            else
            {
                if(aType instanceof IntType && bType instanceof IntType)
                {
                    ExprSTO exprSTO = new ExprSTO(a.getName() + " != " + b.getName(), new BoolType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + " != " + b.getName());
                    MyParser.m_acg.writeRelation(a, b, "" + MyParser.offset, "!=");
                    return exprSTO;
                }
                else
                {
                    ExprSTO exprSTO = new ExprSTO(a.getName() + " != " + b.getName(), new BoolType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + " != " + b.getName());
                    MyParser.m_acg.writeRelationFloat(a, b, "" + MyParser.offset, "!=");
                    return exprSTO;
                }
            }
        }
        else if((aType instanceof BoolType && bType instanceof BoolType))
        {
            if(a instanceof ConstSTO && b instanceof ConstSTO)
            {
                boolean result = ((ConstSTO) a).getBoolValue() != (((ConstSTO) b).getBoolValue());
                if(result) {
                    return new ConstSTO("true", new BoolType(), 1);
                }
                else
                {
                    return new ConstSTO("false", new BoolType(), 0);
                }
            }
            else
            {
                ExprSTO exprSTO = new ExprSTO(a.getName() + " != " + b.getName(), new BoolType());
                MyParser.offset -= 4;
                exprSTO.setOffset("" + MyParser.offset);
                exprSTO.setBase("%fp");
                MyParser.m_acg.writeComment(a.getName() + " != " + b.getName());
                MyParser.m_acg.writeRelation(a, b, "" + MyParser.offset, "!=");
                return exprSTO;
            }
        }
        else if(aType instanceof PointerType && bType instanceof PointerType)
        {
            if(aType instanceof NullPointerType || bType instanceof NullPointerType)
            {
                ExprSTO exprSTO = new ExprSTO(a.getName() + " != " + b.getName(), new BoolType());
                MyParser.offset -= 4;
                exprSTO.setOffset("" + MyParser.offset);
                exprSTO.setBase("%fp");
                MyParser.m_acg.writeComment(a.getName() + " != " + b.getName());
                MyParser.m_acg.writeRelation(a, b, "" + MyParser.offset, "!=");
                return exprSTO;
            }
            else
            {
                if(aType.isEquivalentTo(bType))
                {
                    ExprSTO exprSTO = new ExprSTO(a.getName() + " != " + b.getName(), new BoolType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + " != " + b.getName());
                    MyParser.m_acg.writeRelation(a, b, "" + MyParser.offset, "!=");
                    return exprSTO;
                }
                else
                {
                    return new ErrorSTO("OperandsTypeNotEqual");
                }
            }
        }
        else
        {
            if(aType instanceof PointerType || bType instanceof PointerType)
            {
                return new ErrorSTO("OperandsTypeNotEqual");
            }
            else {
                return new ErrorSTO("errorComparison");
            }
        }
    }
}

/**
 * Created by Dennis on 4/9/2015.
 */
class PostOp extends UnaryOp{
    public PostOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a)
    {
        Type aType = a.getType();

        if(!(aType instanceof NumericType || aType instanceof PointerType))
        {
            return new ErrorSTO("TypeIncorrect");
        }
        else
        {
            if(!(a.isModLValue()))
            {
                return new ErrorSTO("notModLVal");
            }
            else
            {
                // later on need to access the variable
                if(aType instanceof IntType)
                {
                    ExprSTO exprSTO = new ExprSTO(super.getOpName()+a.getName(), new IntType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + super.getOpName());
                    MyParser.m_acg.writeIncOp(a, "" + MyParser.offset, super.getOpName(), false);
                    return exprSTO;
                }
                else if(aType instanceof FloatType)// float type or PointerType
                {
                    ExprSTO exprSTO = new ExprSTO(super.getOpName()+a.getName(), new FloatType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset(""+MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + super.getOpName());
                    MyParser.m_acg.writeIncOpFloat(a, "" + MyParser.offset, super.getOpName(), false);
                    return exprSTO;
                }
                else
                {
                    ExprSTO exprSTO = new ExprSTO(super.getOpName()+a.getName(), new IntType());
                    MyParser.offset -= 4;
                    exprSTO.setOffset("" + MyParser.offset);
                    exprSTO.setBase("%fp");
                    MyParser.m_acg.writeComment(a.getName() + super.getOpName());
                    MyParser.m_acg.writeIncOp(a, "" + MyParser.offset, super.getOpName(), false);
                    return exprSTO;
                }
            }
        }
    }
}

//---------------------------------------------------------------------
// CSE 131 Reduced-C Compiler Project
// Copyright (C) 2008-2015 Garo Bournoutian and Rick Ord
// University of California, San Diego
//---------------------------------------------------------------------

abstract class STO
{
	private String m_strName;
	private Type m_type;
	private boolean m_isAddressable;
	private boolean m_isModifiable;
	private String fullName;
	private boolean dereference = false;
	private String offset;
	private String base;
	private boolean accessFrmArray = false;
	private STO ref = null;
	public boolean isPointer = false;
	public boolean isStatic = false;
	public int strucOffset = 0;
	public boolean isExtern = false;

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO(String strName)
	{
		this(strName, null);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO(String strName, Type typ)
	{
		setName(strName);
		setType(typ);
		setIsAddressable(false);
		setIsModifiable(false);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String getName()
	{
		return m_strName;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	private void setName(String str)
	{
		m_strName = str;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Type getType()
	{
		return m_type;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	private void setType(Type type)
	{
		m_type = type;
	}

	//----------------------------------------------------------------
	// Addressable refers to if the object has an address. Variables
	// and declared constants have an address, whereas results from 
	// expression like (x + y) and literal constants like 77 do not 
	// have an address.
	//----------------------------------------------------------------
	public boolean getIsAddressable()
	{
		return m_isAddressable;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void setIsAddressable(boolean addressable)
	{
		m_isAddressable = addressable;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean getIsModifiable()
	{
		return m_isModifiable;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void setIsModifiable(boolean modifiable)
	{
		m_isModifiable = modifiable;
	}

	//----------------------------------------------------------------
	// A modifiable L-value is an object that is both addressable and
	// modifiable. Objects like constants are not modifiable, so they 
	// are not modifiable L-values.
	//----------------------------------------------------------------
	public boolean isModLValue()
	{
		return getIsModifiable() && getIsAddressable();
	}

	// Added Method
	public boolean isNonModLValue() {
		if(m_isAddressable == true && m_isModifiable == false)
			return true;
		else
			return false;
	}

	// Added Method
	public boolean isRValue()
	{
		if(m_isAddressable == false && m_isModifiable == false)
			return true;
		else
			return false;
	}

	public void setOffset(String offset)
	{
		this.offset = offset;
	}

	public String getOffset()
	{
		return this.offset;
	}

	public void setBase(String base)
	{
		this.base = base;
	}

	public String getBase()
	{
		return this.base;
	}

	public boolean isAccessFromArray()
	{
		return accessFrmArray;
	}

	public void setAccessFromArray(boolean access)
	{
		accessFrmArray = access;
	}

	//----------------------------------------------------------------
	//	It will be helpful to ask a STO what specific STO it is.
	//	The Java operator instanceof will do this, but these methods 
	//	will allow more flexibility (ErrorSTO is an example of the
	//	flexibility needed).
	//----------------------------------------------------------------
	public boolean isVar() { return false; }
	public boolean isConst() { return false; }
	public boolean isExpr() { return false; }
	public boolean isFunc() { return false; }
	public boolean isStructdef() { return false; }
	public boolean isError() { return false; }
	public boolean isThis() { return false; }

	//to be overide
	public void setRef(boolean b) {
	}

	public boolean isRef(){
		return false;
	}

	public void setFullName(String s){
		fullName = s;
	}
	public String getFullName(){
		return fullName;
	}
	public void setDereference(boolean b){
		dereference = b;
	}
	public boolean getDeference(){
		return dereference;
	}
	public STO getRefSTO() { return ref; }
	public void setRefSTO(STO st) { ref = st; }
}

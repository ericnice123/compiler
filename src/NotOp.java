/**
 * Created by Tsung-Huan on 4/9/15.
 */
class NotOp extends UnaryOp {
    public NotOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a)
    {
        Type aType = a.getType();

        if(!(aType instanceof BoolType)) {
            return new ErrorSTO(aType.getName());
        }
        else
        {
            if(a instanceof ConstSTO)
            {
                if(((ConstSTO) a).getBoolValue()) {
                    return new ConstSTO("false", new BoolType(), 0);
                }
                else {
                    return new ConstSTO("new", new BoolType(), 1);
                }
            }
            else {
                ExprSTO exprSTO = new ExprSTO("!"+a.getName(), new BoolType());
                MyParser.offset -= 4;
                exprSTO.setOffset("" + MyParser.offset);
                exprSTO.setBase("%fp");
                MyParser.m_acg.writeComment("!"+a.getName());
                MyParser.m_acg.writeNotOp(a, "" + MyParser.offset);
                return exprSTO;
            }
        }
    }
}

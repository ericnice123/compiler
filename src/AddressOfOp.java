/**
 * Created by Tsung-Huan on 4/22/15.
 */
class AddressOfOp extends CompositeOp {
    public AddressOfOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a)
    {
        if(!a.getIsAddressable())
        {
            return new ErrorSTO("notAddressable");
        }

        Type aType = a.getType();
        PointerType pt = new PointerType();
        pt.setElementType(aType);
        pt.setName("&"+a.getName());
        ExprSTO result = new ExprSTO(pt.getName(), pt);
        MyParser.offset -= 4;
        result.setOffset(""+MyParser.offset);
        result.setBase("%fp");
        result.setIsAddressable(false);
        result.setIsModifiable(false);
        MyParser.m_acg.writeComment("&" + a.getName());
        MyParser.m_acg.writeAddressOf(a, result);
        return result;
    }
}

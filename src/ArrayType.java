/**
 * Created by Tsung-Huan on 4/14/15.
 */
class ArrayType extends CompositeType {

    Type elementType;
    int dimension;

    ArrayType(int size)
    {
        super("ArrayType", size);
    }

    public void setElementType(Type t)
    {
        elementType = t;
    }

    public void setDimension(int d)
    {
        dimension = d;
    }

    public Type getElementType()
    {
        return elementType;
    }

    public int getDimension()
    {
        return dimension;
    }

    public void setName(String name)
    {
        super.setName(name);
    }

    public void setSize(int i )
    {
        super.setSize(i);
    }

    public boolean isAssignableTo(Type t)
    {
        if(t instanceof ArrayType)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public boolean isEquivalentTo(Type t){
        if(t instanceof ArrayType){
            if(getElementType().isEquivalentTo(((ArrayType) t).getElementType())){
                if(getDimension() == ((ArrayType) t).getDimension()){
                    return true;
                }
            }
        }
        return false;
    }
}

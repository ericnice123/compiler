/**
 * Created by Tsung-Huan on 4/9/15.
 */
class AndOp extends BooleanOp{
    public AndOp(String opName)
    {
        super(opName);
    }

    public STO checkOperands(STO a, STO b)
    {
        Type aType = a.getType();
        Type bType = b.getType();

        if(aType instanceof BoolType && bType instanceof BoolType)
        {
            // return expression thats boolean type
            if(a instanceof ConstSTO && b instanceof ConstSTO)
            {
                boolean result = ((ConstSTO) a).getBoolValue() && ((ConstSTO) b).getBoolValue();
                if(result) {
                    return new ConstSTO("" + result, new BoolType(), 1);
                }
                else
                {
                    return new ConstSTO(""+result, new BoolType(), 0);
                }
            }
            else
            {
                ExprSTO exprSTO = new ExprSTO(a.getName() + " && " + b.getName(), new BoolType());
                MyParser.offset -= 4;
                exprSTO.setOffset("" + MyParser.offset);
                exprSTO.setBase("%fp");
                MyParser.m_acg.writeBoolExpr(a, b, "" + MyParser.offset, "&&");
                return exprSTO;
            }
        }
        else
        {
            if(!(aType instanceof BoolType))
            {
                return new ErrorSTO(aType.getName());
            }
            else
            {
                return new ErrorSTO(bType.getName());
            }
        }
    }
}

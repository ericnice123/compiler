TESTS=P2Test/*


make clean
make new


if [ -f differences ]; then
  rm differences
fi

touch differences
diffCounter=0
sameCounter=0

for f in $TESTS
do
  echo "Processing $(basename $f) file..."
  
  ./RC $f
  make compile > compilelog
  ./a.out > myfile
  testrunner_client $f
  make compile > compilelog
  ./a.out > testfile

  cmp -s myfile testfile

  if [ $? -eq 1 ]; then
    echo "\e[31mDifference in file $(basename $f)\e[0m\n"
    echo ">>>>>>>>>>>>>>> Start of file: $(basename $f) <<<<<<<<<<<<<\n" >> differences
    diff myfile testfile >> differences
    echo "\n>>>>>>>>>> End of file: $(basename $f) <<<<<<<" >> differences
    echo "\n-----------------------------------------------\n" >> differences
    diffCounter=$((diffCounter+1))
  else
    sameCounter=$((sameounter+1))
  fi

  rm a.out
done

echo "$diffCounter files have differences" >> differences
echo "$sameCounter files have passed tests" >> differences

rm myfile
rm testfile

